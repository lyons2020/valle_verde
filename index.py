# Módulos App Web
from flask import Flask, redirect, url_for
from views.sesion import view_sesion
from views.mantenimiento.producto import view_producto
from views.usuario import view_colaborador
from views.sistema.menu import view_menu
from views.sistema.perfil import view_perfil
from views.sistema.menu_perfil import view_menu_perfil
from views.sistema.usuario_perfil import view_usuario_perfil
from views.transaccion.venta import view_venta
from views.transaccion.compra import view_compra
from views.transaccion.traslado import view_traslado
from views.mantenimiento.proveedor import view_proveedor
from views.mantenimiento.provincia import view_provincia
from views.mantenimiento.distrito import view_distrito
from views.services.reniec import view_services_reniec
from views.mantenimiento.cliente import view_cliente
from views.mantenimiento.almacen import view_almacen
from views.transaccion.venta_detalle import view_venta_detalle
from views.mantenimiento.stock_almacen import view_stock_almacen
from views.reports.report import view_reporte
# Módulos WS
from services.sesion import ws_sesion
from services.producto import ws_producto
from services.venta import ws_venta

app = Flask(__name__)

# Crear una clave para la gestión de las sesiones de usuario
app.secret_key = "mycretetkey"

# Registrar modulos App Web
app.register_blueprint(view_sesion)
app.register_blueprint(view_producto)
app.register_blueprint(view_colaborador)
app.register_blueprint(view_menu)
app.register_blueprint(view_perfil)
app.register_blueprint(view_menu_perfil)
app.register_blueprint(view_usuario_perfil)
app.register_blueprint(view_venta)
app.register_blueprint(view_compra)
app.register_blueprint(view_traslado)
app.register_blueprint(view_proveedor)
app.register_blueprint(view_provincia)
app.register_blueprint(view_distrito)
app.register_blueprint(view_services_reniec)
app.register_blueprint(view_cliente)
app.register_blueprint(view_almacen)
app.register_blueprint(view_venta_detalle)
app.register_blueprint(view_stock_almacen)
app.register_blueprint(view_reporte)
# WS
app.register_blueprint(ws_sesion)
app.register_blueprint(ws_producto)
app.register_blueprint(ws_venta)


@app.route('/')
def home():
    return redirect(url_for('view_sesion.login'))


if __name__ == '__main__':
    app.run(port=3005, debug=True)
