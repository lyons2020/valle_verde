from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, SelectField
from wtforms.validators import ValidationError, DataRequired, Length, Email


class StoreSesionRequest(FlaskForm):
    # Campos de login.html
    email = StringField(
        validators=[
            DataRequired(message='El campo E-mail es requerido.'),
            Email(message='El campo E-mail no tiene el formato correcto.'),
            Length(
                max=50, message='El campo E-mail debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    clave = PasswordField(
        validators=[
            DataRequired(message='El campo Contraseña es requerido.'),
            Length(
                max=32, message='El campo Contraseña debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )
    perfil = StringField(default="")
    # 0:Credenciales Incorrectas; 1:Todo OK
    estadologin = StringField(default="1")
    msjlogin = StringField(default="Ok")
    submit = SubmitField(label=('Iniciar'))
