from flask import Flask
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, ValidationError
#from wtforms.fields.html5 import DateField
from datetime import date


class SearchKardexRequest(FlaskForm):
    almacen = StringField(
        label=('Almacén *'),
        validators=[
            DataRequired(message='El campo Almacén es requerido.')
        ]
    )

    producto = StringField(
        label=('Producto *')
    )

    # date_ini = DateField(
    #     label=('Fecha Inicial *'),
    #     validators=[
    #         DataRequired(message='El campo Fecha Inicial es requerido.')
    #     ],
    #     format='%Y-%m-%d',
    #     default=date.today(),
    # )

    # date_fin = DateField(
    #     label=('Fecha Final *'),
    #     validators=[
    #         DataRequired(message='El campo Fecha Final es requerido.')
    #     ],
    #     format='%Y-%m-%d',
    #     default=date.today()
    # )
    date_now = date.today()

    def validate_date_fin(form, date_ini):
        if form.date_ini.data > form.date_fin.data:
            raise ValidationError(
                "Fecha final no puede ser mayor a Fecha inicial")