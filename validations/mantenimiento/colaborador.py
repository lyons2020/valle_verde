from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import ValidationError, DataRequired, Length, Email


class StoreColaboradorRequest(FlaskForm):
    # Campos de Colaborador Viene de add-edit.html (templates/mantenimiento/colaborador/add-edit.html)
    id = StringField(label=('ID'))

    dni = StringField(
        label=('DNI *'),
        validators=[
            DataRequired(message='El campo DNI es requerido.'),
            Length(
                min=8, max=8, message="El campo DNI debe tener como máximo %(max)d caracteres.")
        ],
        default=""
    )

    paterno = StringField(
        label=('Ape. Paterno *'),
        validators=[
            DataRequired(message='El campo Ape. Paterno es requerido.'),
            Length(
                max=100, message='El campo Ape. Paterno debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    materno = StringField(
        label=('Ape. Materno *'),
        validators=[
            DataRequired(message='El campo Ape. Materno es requerido.'),
            Length(
                max=100, message='El campo Ape. Materno debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    nombre_persona = StringField(
        label=('Nombre *'),
        validators=[
            DataRequired(message='El campo Nombre es requerido.'),
            Length(
                max=100, message='El campo Nombre debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    email = StringField(
        label=('@E-mail *'),
        validators=[
            DataRequired(message='El campo E-mail es requerido.'),
            Email(message='El campo E-mail no tiene el formato correcto.'),
            Length(
                max=100, message='El campo E-mail debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    celular = StringField(
        label=('Celular *'),
        validators=[
            DataRequired(message='El campo Celular es requerido.'),
            Length(min=9, max=9,
                   message='El campo Celular debe tener %(max)d caracteres.')
        ],
        default=""
    )

    direccion = StringField(
        label=('Dirección *'),
        validators=[
            DataRequired(message='El campo Dirección es requerido.'),
            Length(
                max=250, message='El campo Dirección debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    referencia = StringField(
        label=('Referencia *'),
        validators=[
            DataRequired(message='El campo Referencia es requerido.')
        ],
        default=""
    )
