from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Length, Email


class StoreAlmacenRequest(FlaskForm):
    # Campos de Proveedor Viene de proveedor-agregar-editar (templates/mantenimiento/proveedor/proveedor-agregar-editar.html)
    id = StringField(label=('ID'))

    nombre = StringField(
        label=('Nombre *'),
        validators=[
            DataRequired(message='El campo NOMBRE es requerido.'),
            Length(max=100,
                   message='El campo NOMBRE debe tener %(max)d caracteres.')
        ]
    )

    direccion = StringField(
        label=('Dirección *'),
        validators=[
            DataRequired(message='El campo Dirección es requerido.'),
            Length(
                max=250, message='El campo Dirección debe tener como máximo %(max)d caracteres.')
        ]
    )
