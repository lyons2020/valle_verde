from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import ValidationError, DataRequired, Length, Email


class StoreClienteRequest(FlaskForm):
    # Campos de Cliente Viene de add-edit (templates/mantenimiento/cliente/add-edit.html)
    id = StringField(label=('ID'))
    tiene_credito = StringField(label=('Tiene Crédito'))
    total_credito = StringField(label=('Total Crédito'))
    # Datos Persona
    tipodoc = StringField(
        label=('T. Documento *')
    )
    max_length = "0"

    def validate_tipodoc(self, tipodoc):
        if self.id == "0":
            raise ValidationError("El campo Tipo Doc. es requerido.")
        elif tipodoc.data == "1":
            self.max_length = "8"
        elif tipodoc.data == "2":
            self.max_length = "11"
        else:
            self.max_length = "0"
    numero_doc = StringField(
        label=('Número Doc. *'),
        validators=[
            DataRequired(message='El campo Número Doc. es requerido.')
        ],
        default=""
    )
    paterno = StringField(
        label=('Ape. Paterno'),
        validators=[
            Length(
                max=100, message='El campo Ape. Paterno debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )
    materno = StringField(
        label=('Ape. Materno'),
        validators=[
            Length(
                max=100, message='El campo Ape. Materno debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )
    nombre = StringField(
        label=('Nombre *'),
        validators=[
            DataRequired(message='El campo Nombre es requerido.'),
            Length(
                max=100, message='El campo Nombre debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )
    direccion = StringField(
        label=('Dirección *'),
        validators=[
            DataRequired(message='El campo Dirección es requerido.'),
            Length(
                max=250, message='El campo Dirección debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )
    referencia = StringField(
        label=('Referencia '),
        default=""
    )
    email = StringField(
        label=('@E-mail *'),
        validators=[
            DataRequired(message='El campo E-mail es requerido.'),
            Email(message='El campo E-mail no tiene el formato correcto.'),
            Length(
                max=100, message='El campo E-mail debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )
    celular = StringField(
        label=('Celular *'),
        validators=[
            DataRequired(
                message='El campo Celular es requerido.'),
            Length(max=9, message='El campo Celular debe tener %(max)d caracteres.')
        ],
        default=""
    )
