from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Length, Email


class StoreProveedorRequest(FlaskForm):
    # Campos de Proveedor Viene de proveedor-agregar-editar (templates/mantenimiento/proveedor/proveedor-agregar-editar.html)
    id = StringField(label=('ID'))

    ruc = StringField(
        label=('RUC *'),
        validators=[
            DataRequired(message='El campo RUC es requerido.'),
            Length(min=11, max=11,
                   message='El campo RUC debe tener %(max)d caracteres.')
        ],
        default=""
    )

    razon = StringField(
        label=('Razón Social *'),
        validators=[
            DataRequired(message='El campo Razón Social es requerido.'),
            Length(
                max=100, message='El campo Razón Social debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    celular = StringField(
        label=('Celular *'),
        validators=[
            DataRequired(message='El campo Celular es requerido.'),
            Length(max=9,
                   message='El campo Celular debe tener %(max)d caracteres.')
        ],
        default=""
    )

    email = StringField(
        label=('@E-mail *'),
        validators=[
            DataRequired(message='El campo E-mail es requerido.'),
            Email(message='El campo E-mail no tiene el formato correcto.'),
            Length(
                max=100, message='El campo E-mail debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    direccion = StringField(
        label=('Dirección *'),
        validators=[
            DataRequired(message='El campo Dirección es requerido.'),
            Length(
                max=250, message='El campo Dirección debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    # Datos Persona
    dnirepre = StringField(
        label=('Dni *'),
        validators=[
            DataRequired(
                message='El campo DNI de representante es requerido.'),
            Length(
                min=8, max=8, message='El campo DNI de representante debe tener %(max)d caracteres.')
        ],
        default=""
    )

    nombrerepre = StringField(
        label=('Nombre *'),
        validators=[
            DataRequired(
                message='El campo Nombre de representante es requerido.'),
            Length(
                max=100, message='El campo Nombre de representante debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    celularrepre = StringField(
        label=('Celular *'),
        validators=[
            DataRequired(
                message='El campo Celular de representante es requerido.'),
            Length(
                max=9, message='El campo Celular de representante debe tener %(max)d caracteres.')
        ],
        default=""
    )

    emailrepre = StringField(
        label=('@Email *'),
        validators=[
            DataRequired(
                message='El campo E-mail de representante es requerido.'),
            Email(
                message='El campo E-mail de representante no tiene el formato correcto.'),
            Length(
                max=100, message='El campo E-mail de representante debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )

    direccionrepre = StringField(
        label=('Dirección *'),
        validators=[
            DataRequired(
                message='El campo Dirección de representante es requerido.'),
            Length(
                max=250, message='El campo Dirección de representante debe tener como máximo %(max)d caracteres.')
        ],
        default=""
    )
