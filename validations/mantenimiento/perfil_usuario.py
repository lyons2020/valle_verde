from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms.validators import ValidationError, DataRequired, Length, Email


class StorePerfilUsuarioRequest(FlaskForm):
    # Campos de Perfil usuario Viene de add-edit.html (templates/mantenimiento/perfil_usuario/add-edit.html)
    txtid = StringField(label=('ID'))

    txtnombre = StringField(
        label=('Nombre*'),
        validators=[
            DataRequired(message='El campo Nombre de Usuario  es requerido.')
        ]
    )

    txtimg = FileField(
        label=('Imagen Perfil*'),
        validators=[
            FileAllowed(['jpg', 'png'], 'Subir sólo imagenes!')
        ]
    )
