from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import ValidationError, DataRequired, Length, Email


class StoreUsuarioPerfilRequest(FlaskForm):
    # Campos de Usuario Perfil viene de add-edit.html (templates/sistema/usuario_perfil/add-edit.html)
    id = StringField(
        label=('ID'),
        validators=[
            DataRequired(message='El campo ID es requerido.')
        ]
    )
