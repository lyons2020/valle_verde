from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import ValidationError, DataRequired, Length, Email


class StoreMenuRequest(FlaskForm):
    # Campos de Menu Viene de add-edit.html(templates/sistema/menu/add-edit.html)
    id = StringField(label=('ID'))

    nombre = StringField(
        label=('Nombre *'),
        validators=[
            DataRequired(message='El campo Nombre es requerido.'),
            Length(max=50, message='El campo Nombre debe tener %(max)d caracteres.')
        ]
    )

    route = StringField(
        label=('Ruta'),
        validators=[Length(
            max=50, message='El campo Ruta debe tener como máximo %(max)d caracteres.')]
    )
