from flask import Flask, render_template
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import ValidationError, DataRequired


class StoreMenuPerfilRequest(FlaskForm):
    # Campos de Menu Perfil Viene de add-edit.html (templates/sistema/menu_perfil/add-edit.html)
    id = StringField(
        label=('ID'),
        validators=[
            DataRequired(message='El campo ID es requerido.')
        ]
    )
