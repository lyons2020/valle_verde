from flask import Blueprint, request, jsonify
from models.sesion import Sesion
import json
import utils
import jwt
import datetime
from config import SecretKey

ws_sesion = Blueprint('ws_sesion', __name__)


@ws_sesion.route('/login/auth/ws', methods=['POST'])
def auth_ws():
    if request.method == 'POST':
        email = request.form['email']
        clave = request.form['clave']

        objSesion = Sesion(email, clave)
        rptaJSON = objSesion.iniciarSesion()
        datos = json.loads(rptaJSON)  # Convetir el JSON a un Array

        if datos["status"] == True:  # Ha iniciado correctamente la sesión
            # Obtener id de usuario para almacenarlo en token
            usuario_id = datos['data']['id']
            # Generar token
            token = jwt.encode({'usuario_id': usuario_id, 'exp': datetime.datetime.utcnow(
            ) + datetime.timedelta(seconds=1*60*60)}, SecretKey.JWT_SECRET_KEY)
            # Almacenar token en BD
            objSesion.actualizarToken(token, usuario_id)
            # Agregar token a resultado que devolverá el WS
            datos['data']['token'] = token.encode().decode('utf-8')
            # Imprimir el resultado del WS
            return jsonify(datos), 200
        else:  # Ocurrió en inicio de sesión
            return jsonify(datos), 401
