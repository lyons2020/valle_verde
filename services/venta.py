from flask import Blueprint, request, jsonify
from models.transaccion.venta import Venta
import json
import validarJWT
ws_venta = Blueprint('ws_venta', __name__)


@ws_venta.route('/ventas/save/ws', methods=['POST'])
@validarJWT.token_requerido
def ventas_save_ws():
    print(request.form['usuario_id_registro'])
    # Recibir los datos de la venta
    cliente_id = request.form['cliente_id']
    tipo_comprobante_id = request.form['tc_id']
    nser = request.form['serie']
    ndoc = request.form['ndoc']
    fdoc = request.form['fdoc']
    sub_total = request.form['subtotal']
    igv = request.form['igv']
    total = request.form['totalneto']
    porcentaje_igv = request.form['porcentajeigv']
    usuario_id_registro = request.form['usuario_id_registro']
    almacen_id = request.form['almacen_id']
    # Recibir los datos del detalle de venta
    detalle = request.form['detalleventa']

    objVta = Venta(0, cliente_id, tipo_comprobante_id, nser, ndoc, fdoc,
                   sub_total, igv, total, porcentaje_igv, usuario_id_registro, almacen_id, detalle)
    rptaJSON = objVta.insertar()
    datos_venta = json.loads(rptaJSON)

    if datos_venta['status'] == True:
        return jsonify(datos_venta), 200
    else:
        return jsonify(datos_venta), 500
