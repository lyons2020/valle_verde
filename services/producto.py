from flask import Blueprint, request, jsonify
from models.mantenimiento.producto import Producto
import json
import validarJWT
ws_producto = Blueprint('ws_producto', __name__)


@ws_producto.route('/productos/ws', methods=['POST'])
@validarJWT.token_requerido
def productos_ws():
    objP = Producto()
    rptaJSON = objP.listar()
    datosP = json.loads(rptaJSON)
    return jsonify(datosP), 200


@ws_producto.route('/productos/save/ws', methods=['POST'])
@validarJWT.token_requerido
def productos_save_ws():
    # Captura de datos del Ws
    id = request.form['id']
    nombre = request.form['nombre']
    precio = request.form['precio']
    categoria_id = request.form['categoria_id']
    objP = Producto(id, nombre, precio, categoria_id)

    if id == '0':
        # Llamar al método insertar
        rptaJSON = objP.insertar()
    else:
        # Llamar al método editar
        rptaJSON = objP.editar()

    # Convertir el resultado JSON String JSON Array
    datosP = json.loads(rptaJSON)
    if datosP['status'] == True:
        return jsonify(datosP), 200
    else:
        return jsonify(datosP), 500


@ws_producto.route('/productos/leer/ws', methods=['POST'])
@validarJWT.token_requerido
def productos_leer_ws():
    # Captura de datos del WS
    id = request.form['id']
    objP = Producto()

    # Llamar al método leer
    rptaJSON = objP.leer(id)

    # Convertir el resultado JSON String JSON Array
    datosP = json.loads(rptaJSON)
    return jsonify(datosP), 200


@ws_producto.route('/productos/delete/ws', methods=['POST'])
@validarJWT.token_requerido
def productos_eliminar_ws():
    # Captura de datos del WS
    id = request.form['id']
    objP = Producto()

    # Llamar al método eliminar
    rptaJSON = objP.eliminar(id)

    # Convertir el resultado JSON String JSON Array
    datosP = json.loads(rptaJSON)
    return jsonify(datosP), 200
