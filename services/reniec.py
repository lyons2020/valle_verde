import requests
import json


class Reniec:

    def __init__(self, dni=None):
        self.url = "http://www.facturacionsunat.com/vfpsws/vfpsconsbsapi.php?dni=" + \
            dni+"&token=87290E49D50B519&format=json"
        self.dni = dni

    def buscar_dni(self):
        response = requests.get(self.url)
        print(response)
        if(response.text == "Numero de DNI no encontrado. Intente nuevamente."):
            return json.dumps({"status": False, "data": {"error": response.text + " O ingrese manualmente los datos de persona."}})
        else:
            return json.dumps({"status": False, "data": response.json()})
