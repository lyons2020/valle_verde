import requests
import json


class Sunat:

    def __init__(self, ruc=None):
        self.url = "https://ruc.com.pe/api/v1/consultas/"
        self.ruc = ruc
        self.headers = {"Content-Type": "application/json"}

    def buscar_ruc(self):
        # fake email: yegebe6281@animex98.com (token) - https://ruc.com.pe/ - https://temp-mail.org/es/view/cc5722ed90f20a5fcd22806dc7d0f2d4  #123456789
        _json = {
            "token": "c2e2c4f0-03dc-44df-a388-56e4fe78e32a-1b757ec8-9fef-404f-bc8a-0ad34017820a", "ruc": self.ruc}
        response = requests.post(
            self.url, data=json.dumps(_json), headers=self.headers)

        return json.dumps({"status": False, "data": response.json()})
