from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
import hashlib
import json


class Usuario():
    def __init__(self, id=None, nombre=None, email=None, clave=None, img='0/anonimo.png', persona_id=None):
        self.id = id
        self.nombre = nombre
        self.email = email
        self.clave = (hashlib.md5('123'.encode())).hexdigest()
        self.img = img  # ruta img para nuevo colaborador
        self.persona_id = persona_id

    def listar(self):
        cursor = db().open.cursor()
        cursor.execute('SELECT * FROM lista_colaborador')
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def insertar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "INSERT INTO usuario (nombre,email,clave,img,persona_id) values (%s,%s,%s,%s,%s)"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.nombre, self.email,
                           self.clave, self.img, self.persona_id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente", "id": cursor.lastrowid})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def leer(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute(
            'SELECT usu.id,usu.nombre AS nombre_usuario,usu.email,per.numero_documento as dni,per.apellido_paterno,per.apellido_materno,per.nombre,per.direccion,per.referencia,per.celular,per.distrito_id,dep.id AS departamento_id,prov.id AS provincia_id FROM usuario usu INNER JOIN persona per ON(usu.persona_id=per.id) INNER JOIN distrito dis ON(per.distrito_id=dis.id) INNER JOIN provincia prov ON(prov.id=dis.provincia_id) INNER JOIN departamento dep ON(dep.id=prov.departamento_id) WHERE usu.id=%s', [id])

        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def editar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "update usuario set nombre=%s, email=%s, img=%s where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.nombre, self.email,
                           self.img, self.id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def eliminar(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Preparar la sentencia SQL Delete
        sql = "delete from usuario where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Eliminado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite buscar por id - tabla usuario
    def buscar_id(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute(
            'select * from usuario WHERE id=%s', [id])

        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def actualizar_perfil(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "update usuario set nombre=%s, img=%s where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.nombre,
                           self.img, self.id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def verificarClave(self, clave, id):
        cursor = db().open.cursor()
        # Para encriptar la clave actual del formulario
        claveMD5 = (hashlib.md5(clave.encode())).hexdigest()
        cursor.execute(
            'select * from usuario where clave=%s and id=%s', [claveMD5, id])
        datos = cursor.fetchone()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos})
        else:
            return json.dumps({"status": False, "data": "Clave es incorrecta"})

    def actualizar_clave(self, clavenueva, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "update usuario set clave=%s where id=%s"
        claveMD5 = (hashlib.md5(clavenueva.encode())).hexdigest()

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [claveMD5, id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()
