from connection import Connection as db  # conectar a base datos
from JsonEncoder import CustomJsonEncoder
import json


class Departamento():
    # Definir todos los campos de la tabla departamento de la BD
    def __init__(self, id=None, nombre=None):
        self.id = id
        self.nombre = nombre

    # Permite listar todas los departamentos
    def listar(self):
        cursor = db().open.cursor()
        cursor.execute('SELECT * FROM departamento')
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})
