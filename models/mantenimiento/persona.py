from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
import json


class Persona():
    # Definir todos los campos de la tabla persona de la BD
    # tipo_usuario_id (colaborador=1; proveedor:2; cliente:3)
    # tipo_documento_id (dni:1;ruc:2;ce:3)
    def __init__(self, id=None, numero_documento=None, apellido_paterno="", apellido_materno="", nombre=None, direccion=None, referencia="", email="", tipo_documento_id=2, celular=None, distrito_id=None, tipo_usuario=2):
        self.id = id
        self.numero_documento = numero_documento
        self.apellido_paterno = apellido_paterno
        self.apellido_materno = apellido_materno
        self.nombre = nombre
        self.direccion = direccion
        self.referencia = referencia
        self.email = email
        self.tipo_documento_id = tipo_documento_id
        self.celular = celular
        self.distrito_id = distrito_id
        self.tipo_usuario = tipo_usuario

    # Permite insertar datos a la tabla persona
    def insertar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "insert into persona (numero_documento,apellido_paterno,apellido_materno,nombre,direccion,referencia,email,tipo_documento_id,celular,distrito_id,tipo_usuario) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        try:
            print(sql)
            # Ejecutar el comando SQL
            cursor.execute(sql, [
                self.numero_documento, self.apellido_paterno, self.apellido_materno, self.nombre,
                self.direccion, self.referencia, self.email, self.tipo_documento_id, self.celular,
                self.distrito_id, self.tipo_usuario
            ])

            # Confirmar los cambios en la base datos
            con.commit()
            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": cursor.lastrowid})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite actualizar un registro de la tabla persona
    def editar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Preparar la sentencia SQL Insert
        sql = "update persona set apellido_paterno=%s, apellido_materno=%s,nombre=%s, direccion=%s, referencia=%s, email=%s, celular=%s, distrito_id=%s where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.apellido_paterno, self.apellido_materno, self.nombre,
                                 self.direccion, self.referencia, self.email, self.celular, self.distrito_id, self.id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite eliminar un registro de la tabla persona
    def eliminar(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "delete from persona where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Eliminado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite buscar por nroDoc (ruc-dni) - tabla persona
    def buscar(self, nrodoc):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        #cursor.execute('select * from proveedor where id=%s', [id])
        cursor.execute(
            'select * from persona WHERE numero_documento=%s', [nrodoc])

        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})
