from connection import Connection as db  # conectar a base datos
from JsonEncoder import CustomJsonEncoder
import json


class Provincia():
    # Definir todos los campos de la tabla provincia de la BD
    def __init__(self, id=None, nombre=None, departamento_id=None):
        self.id = id
        self.nombre = nombre
        self.departamento_id = departamento_id

    # Permite listar todas las provincias
    def listar(self):
        cursor = db().open.cursor()
        cursor.execute('SELECT * FROM provincia')
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    # Permite listar todas las provincias por departamento seleccionado
    def listar_prov_dpto(self, id):  # Id= Id que viene al hacer click en el combo DPTO
        cursor = db().open.cursor()
        cursor.execute(
            'select * from provincia where departamento_id=%s', [id])
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": []})
