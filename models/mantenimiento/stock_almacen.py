from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
import json


class StockAlmacen():
    # ******************Leo Chimoy*****************************
    # Definir todos los campos de la tabla producto de la BD
    def __init__(self, id=None, almacen_id=None, producto_id=None, stock=None):
        self.id = id
        self.almacen_id = almacen_id
        self.producto_id = producto_id
        self.stock = stock

    def leer(self, producto_id, almacen_id):
        # Abrir la conexion a la BD
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta
        cursor.execute(
            'select * from stock_almacen where producto_id=%s and almacen_id =%s ', [producto_id, almacen_id])

        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el sursor y la conexion
        cursor.close()
        con.close()

        # Retornar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def prodalm(self, almacen_id):
        # Abrir la conexion a la BD
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta
        cursor.execute(
            'SELECT sa.id AS codigo, sa.almacen_id, sa.producto_id, p.nombre FROM stock_almacen sa inner join producto p ON (p.id = sa.producto_id) WHERE almacen_id=%s ', [almacen_id])

        # Capturar los datos
        datos = cursor.fetchall()

        # Cerrar el sursor y la conexion
        cursor.close()
        con.close()

        # Retornar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})


# ******************Luis Azalde*****************************
    # Permite listar productos por almacén

    def listar_prods_almacen(self, almacen_id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute(
            'SELECT sa.id AS codigo,sa.producto_id,sa.stock,p.nombre FROM stock_almacen sa INNER JOIN producto p ON(p.id=sa.producto_id) WHERE sa.almacen_id=%s ORDER BY p.nombre', [almacen_id])

        # Capturar los datos
        datos = cursor.fetchall()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})
