from connection import Connection as db  # conectar a base datos
from JsonEncoder import CustomJsonEncoder
import json


class Distrito():
    # Definir todos los campos de la tabla distrito de la BD
    def __init__(self, id=None, nombre=None, provincia_id=None):
        self.id = id
        self.nombre = nombre
        self.provincia_id = provincia_id

    # Permite listar todas los distritos
    def listar(self):
        cursor = db().open.cursor()
        cursor.execute('SELECT * FROM distrito')
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    # Permite listar todas los distritos por provincia seleccionada
    # id: id que viene al hacer click en el combo Provincia
    def listar_dist_prov(self, id):
        cursor = db().open.cursor()
        cursor.execute('select * from distrito where provincia_id=%s', [id])
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": []})
