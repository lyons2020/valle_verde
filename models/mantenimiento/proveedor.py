from connection import Connection as db  # conectar a base datos
from JsonEncoder import CustomJsonEncoder
import json


class Proveedor():
    # Definir todos los campos de la tabla proveedor de la BD
    def __init__(self, id=None, dni_repre=None, nombre_repre=None, dire_repre=None, email_repre=None, cel_repre=None, persona_id=None):
        self.id = id
        self.dni_repre = dni_repre
        self.nombre_repre = nombre_repre
        self.dire_repre = dire_repre
        self.email_repre = email_repre
        self.cel_repre = cel_repre
        self.persona_id = persona_id

    # Permite listar datos de proveedor mediante una Vista BD lista_proveedor
    def listar(self):
        cursor = db().open.cursor()
        cursor.execute('SELECT * FROM lista_proveedor')
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    # Permite insertar datos a la tabla proveedor
    def insertar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "insert into proveedor (nro_doc_repre_legal,nombre_repre_legal,direc_repre_legal,email_repre_legal,cel_repre_legal,persona_id) values (%s,%s,%s,%s,%s,%s)"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.dni_repre, self.nombre_repre,
                           self.dire_repre, self.email_repre, self.cel_repre, self.persona_id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite leer datos de la tabla proveedor,persona,distrito,provincia y dpto para luego
    # Actualiza en tabla proveedor
    def leer(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        #cursor.execute('select * from proveedor where id=%s', [id])
        cursor.execute(
            'SELECT pro.*,per.numero_documento AS ruc, per.nombre AS razon_social,per.direccion,per.email,per.celular,per.distrito_id,dep.id AS departamento_id,prov.id AS provincia_id FROM proveedor pro INNER JOIN persona per ON(per.id=pro.persona_id) INNER JOIN distrito dis ON(dis.id=per.distrito_id) INNER JOIN provincia prov ON(prov.id=dis.provincia_id) INNER JOIN departamento dep ON(dep.id=prov.departamento_id) WHERE pro.id=%s', [id])
        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    # Permite actualizar un registro de la tabla proveedor
    def editar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "update proveedor set nro_doc_repre_legal=%s, nombre_repre_legal=%s, direc_repre_legal=%s, email_repre_legal=%s, cel_repre_legal=%s where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.dni_repre, self.nombre_repre, self.dire_repre,
                           self.email_repre, self.cel_repre, self.id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite eliminar un registro de la tabla proveedor
    def eliminar(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "delete from proveedor where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Eliminado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite buscar por id - tabla proveedor
    def buscar_id(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute(
            'select * from proveedor WHERE id=%s', [id])

        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

# Leo Chimoy
    def listarProveedorCompra(self):
        cursor = db().open.cursor()
        cursor.execute('select pr.id,pe.numero_documento AS ruc,pe.nombre AS razon_social, pe.direccion,pe.distrito_id  from proveedor pr INNER JOIN persona pe ON(pe.id=pr.persona_id) order by pe.nombre')
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos})
        else:
            return json.dumps({"status": False, "data": "No hay datos"})
