from connection import Connection as db  # conectar a base datos
from JsonEncoder import CustomJsonEncoder
import json


class Cliente():
    # Definir todos los campos de la tabla cliente de la BD
    def __init__(self, id=None, tiene_credito=None, total_credito=None, persona_id=None):
        self.id = id
        self.tiene_credito = tiene_credito
        self.total_credito = total_credito
        self.persona_id = persona_id

    def listarClienteVenta(self):
        cursor = db().open.cursor()
        cursor.execute("SELECT cli.id,CONCAT(per.apellido_paterno,' ',per.apellido_materno,' ',per.nombre) AS `nombre` FROM cliente cli INNER JOIN persona per ON(per.id=cli.persona_id) ORDER BY 1")
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos})
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    # Permite listar datos de cliente mediante una Vista BD lista_cliente

    def listar(self):
        cursor = db().open.cursor()
        cursor.execute('SELECT * FROM lista_cliente')
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    # Permite insertar datos a la tabla cliente
    def insertar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "insert into cliente (tiene_credito,total_credito,persona_id) values (%s,%s,%s)"

        try:
            # Ejecutar el comando SQL
            cursor.execute(
                sql, [self.tiene_credito, self.total_credito, self.persona_id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite leer datos de la tabla cliente,persona,distrito,provincia y dpto para luego
    # Actualiza en tabla cliente
    def leer(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute(
            'SELECT cli.*,per.numero_documento,per.apellido_paterno,per.apellido_materno,per.nombre,per.direccion,per.referencia,per.email,tdo.id AS tipo_doc_id,per.celular,per.distrito_id,dep.id AS departamento_id,prov.id AS provincia_id FROM cliente cli INNER JOIN persona per ON(per.id=cli.persona_id) INNER JOIN tipo_documento tdo ON(tdo.id=per.tipo_documento_id) INNER JOIN distrito dis ON(dis.id=per.distrito_id) INNER JOIN provincia prov ON(prov.id=dis.provincia_id) INNER JOIN departamento dep ON(dep.id=prov.departamento_id) WHERE cli.id=%s', [id])
        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    # Permite actualizar un registro de la tabla proveedor
    def editar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "update cliente set tiene_credito=%s, total_credito=%s where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(
                sql, [self.tiene_credito, self.total_credito, self.id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite eliminar un registro de la tabla proveedor
    def eliminar(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "delete from cliente where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Eliminado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    # Permite buscar por id - tabla cliente
    def buscar_id(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute(
            'select * from cliente WHERE id=%s', [id])

        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})
