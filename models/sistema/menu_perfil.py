from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
import json


class MenuPerfil():
    def __init__(self, id=None, menu_id=None, perfil_id=None, estado=1):
        self.id = id
        self.menu_id = menu_id
        self.perfil_id = perfil_id
        self.estado = estado

    def listar(self):
        cursor = db().open.cursor()
        cursor.execute("SELECT mp.id AS id,pe.nombre AS perfil,me.nombre AS menu,mp.estado FROM menu_perfil mp INNER JOIN menu me ON me.id=mp.menu_id INNER JOIN perfil pe ON pe.id=mp.perfil_id where mp.estado=1 ORDER BY pe.nombre")
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def insertar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()
        # Prepapar la sentencia SQL Insert
        sql = "INSERT INTO menu_perfil (menu_id,perfil_id) values (%s,%s)"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.menu_id, self.perfil_id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def leer(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute(
            'select * from menu_perfil where estado=1 and id=%s', [id])

        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def editar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "update menu_perfil set menu_id=%s,perfil_id=%s where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.menu_id, self.perfil_id, self.id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def eliminar(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "delete from menu_perfil where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Eliminado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def listar_menu_x_perfil(self, perfil_id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute(
            'SELECT * FROM menu_perfil WHERE perfil_id=%s', [perfil_id])

        # Capturar los datos
        datos = cursor.fetchall()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})
