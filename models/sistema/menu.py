from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
import json


class Menu():
    def __init__(self, id=None, menu_id=None, nombre=None, route=None, estado=None):
        self.id = id
        self.menu_id = menu_id
        self.nombre = nombre
        self.route = route
        self.estado = estado

    def listar(self):
        cursor = db().open.cursor()
        cursor.execute(
            'SELECT id,menu_id,nombre,route from menu where estado=1')
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def insertar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "INSERT INTO menu (menu_id,nombre,route) values (%s,%s,%s)"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.menu_id, self.nombre, self.route])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def leer(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute('select * from menu where estado=1 and id=%s', [id])

        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def editar(self):
        print(self.route)
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "update menu set menu_id=%s, nombre=%s, route=%s where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(
                sql, [self.menu_id, self.nombre, self.route, self.id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def eliminar(self, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "delete from menu where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Eliminado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def listar_menu_x_perfil(self, perfil_id):
        # Abrir conexión a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute(
            'SELECT a.id as menu_padre,a.nombre AS `menu`,a.route AS route_menu,b.id as menu_hijo,b.nombre AS `submenu`, b.route FROM (SELECT * FROM menu ORDER BY if(menu_id=0,id,menu_id) asc) a LEFT JOIN menu b ON (a.id = b.menu_id) WHERE a.id IN (SELECT mp.menu_id FROM menu_perfil mp WHERE mp.perfil_id=%s) ORDER by a.id,b.nombre', [perfil_id])

        # Capturar los datos
        datos = cursor.fetchall()

        # Cerrar el cursor y la conexión
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})
