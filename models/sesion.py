from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
import json


class Sesion():
    # Definir solo los campos e-mail y clave de la tabla usuario de la BD
    def __init__(self, p_email=None, p_clave=None):
        self.email = p_email
        self.clave = p_clave
 # Permite validar el parametro de email y clave de la bd para iniciar sesion

    def iniciarSesion(self):
        cursor = db().open.cursor()
        cursor.execute(
            'select id,nombre,email,img,estado,persona_id from usuario where email = %s and clave = %s', [self.email, self.clave])
        datos = cursor.fetchone()
        cursor.close()
        if datos:
            # Validar si usuario tien estado 1 (Activo)
            if datos['estado'] == '1':
                return json.dumps({"status": True, "data": datos})
            else:
                return json.dumps({"status": False, "data": "Usuario Inactivo."})
        else:
            return json.dumps({"status": False, "data": "Email o clave son incorrectos"})

    def actualizarToken(self, token, id):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Update
        sql = "update usuario set estado_token='1', token=%s where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [token, id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def validarEstadoToken(self, id):
        # Abrir conexi�n a la base de datos
        con = db().open

        # Crear un cursor
        cursor = con.cursor()

        # Se ejecuta la consulta SQL
        cursor.execute('select estado_token from usuario where id = %s', [id])

        # Capturar los datos
        datos = cursor.fetchone()

        # Cerrar el cursor y la conexi�n
        cursor.close()
        con.close()

        # Retonar el resultado
        if datos:
            return json.dumps({"status": True, "data": datos})
        else:
            return json.dumps({"status": False, "data": "Estado de token no encontrado"})
