from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
import json


class VentaDetalle():
    def __init__(self, id=None, venta_id=None, producto_id=None, cantidad=None, precio=None):
        self.id = id
        self.venta_id = venta_id
        self.producto_id = producto_id
        self.cantidad = cantidad
        self.precio = precio

    def search_details(self, venta_id):
        cursor = db().open.cursor()
        cursor.execute(
            'SELECT vd.id,vd.cantidad,vd.precio,p.nombre AS producto FROM venta_detalle vd INNER JOIN producto p ON (p.id=vd.producto_id) WHERE vd.venta_id=%s', [venta_id])
        # Capturar los datos
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})
