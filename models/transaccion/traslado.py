from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
from models.mantenimiento.kardex import Kardex
import json


class Traslado():
    #*************Leo Chimoy*******************#
    def __init__(self, id=None, usuario_id_registro=None, almacen_origen_id=None, almacen_final_id=None, motivo=None, fecha=None, detalle=None, usuario_id_anulacion=None, fecha_hora_anulacion=None, estado=None):
        self.id = id
        self.usuario_id_registro = usuario_id_registro
        self.almacen_origen_id = almacen_origen_id
        self.almacen_final_id = almacen_final_id
        self.motivo = motivo
        self.fecha = fecha
        self.detalle = detalle
        self.usuario_id_anulacion = usuario_id_anulacion
        self.fecha_hora_anulacion = fecha_hora_anulacion
        self.estado = estado

    def listar(self):
        cursor = db().open.cursor()
        cursor.execute('SELECT * FROM vista_traslado_lista')
        datos = cursor.fetchall()  # jala todos los registros
        cursor.close()
        if datos:
            # cls=CustomJsonEncoder cuando encuentra un decimal lo convierte a float
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def insertar(self):
        # Abrir la conexion a la BD
        con = db().open

        # Indicar que los cambios realizados en la BD no se confirman de manera automatica
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Preparar la sentencia SQL insert (tabla traslado)
        sql = "insert into traslado (usuario_id_registro,almacen_origen_id,almacen_final_id,motivo,fecha) values(%s,%s,%s,%s,%s)"

        try:
            # Ejecutar el comando SQL (insertar traslado en Tabla traslado)
            cursor.execute(sql, [self.usuario_id_registro, self.almacen_origen_id,
                           self.almacen_final_id, self.motivo, self.fecha])

            # obtener el id de la Traslado para insertarla en la tabla Traslado_detalle
            self.id = con.insert_id()

            # Grabar en la tabla:Traslado_detalle
            # Recoger el JSON del detalle de la Traslado y colocarlo como JSON Array
            jsonArrayTrasladoDetalle = json.loads(self.detalle)

            # recorrer los elementos del Array jsonArrayTrasladoDetalle
            for det in jsonArrayTrasladoDetalle:
                # Preparar la sentencia SQL insert (tabla traslado_detalle)
                sql = "insert into traslado_detalle (traslado_id,producto_id,cantidad) values(%s,%s,%s)"
                # Ejecutar el comando SQL
                cursor.execute(
                    sql, [self.id, det["producto_id"], det["cantidad"]])

            # actualizar el stock de los productos segun el traslado
            # Obtener nombre de almacen origen
            sqlAlmacenO = "select nombre from almacen where id=%s"
            cursor.execute(sqlAlmacenO, [self.almacen_origen_id])
            datosAlmacenO = cursor.fetchone()  # Obtiene un solo registro
            nombreAlmacenO = datosAlmacenO['nombre']
            # Obtener nombre de almacen destino
            sqlAlmacenF = "select nombre from almacen where id=%s"
            cursor.execute(sqlAlmacenF, [self.almacen_final_id])
            datosAlmacenF = cursor.fetchone()  # Obtiene un solo registro
            nombreAlmacenF = datosAlmacenF['nombre']
            for det2 in jsonArrayTrasladoDetalle:
                # Buscar producto por almacén
                sqlProdAlm = "select * from stock_almacen where almacen_id=%s and  producto_id = %s"
                cursor.execute(
                    sqlProdAlm, [self.almacen_final_id, det2["producto_id"]])
                datosStockAlmProd = cursor.fetchone()  # Obtiene un solo registro

                if datosStockAlmProd:
                    # restar el stock de los productos segun el traslado
                    sql = "update stock_almacen set stock = stock - %s where almacen_id = %s and producto_id = %s"
                    # Ejecutar el comando SQL (tabla stock_almacen)
                    cursor.execute(
                        sql, [det2["cantidad"], self.almacen_origen_id, det2["producto_id"]])

                    # sumar el stock de los productos segun el traslado
                    sql = "update stock_almacen set stock = stock + %s where almacen_id = %s and producto_id = %s"
                    # Ejecutar el comando SQL (tabla stock_almacen)
                    cursor.execute(
                        sql, [det2["cantidad"], self.almacen_final_id, det2["producto_id"]])
                else:
                    # restar el stock de los productos segun el traslado
                    sql = "update stock_almacen set stock = stock - %s where almacen_id = %s and producto_id = %s"
                    # Ejecutar el comando SQL (tabla stock_almacen)
                    cursor.execute(
                        sql, [det2["cantidad"], self.almacen_origen_id, det2["producto_id"]])

                    sql = "insert into stock_almacen (stock,almacen_id,producto_id) values (%s,%s,%s)"
                    # Ejecutar el comando SQL (tabla stock_almacen)
                    cursor.execute(
                        sql, [det2["cantidad"], self.almacen_final_id, det2["producto_id"]])

                # Insertar en kardex salida de mercadería
                # Obtener nombre de producto
                sqlProducto = "select nombre,precio from producto where id=%s"
                cursor.execute(sqlProducto, [det2["producto_id"]])
                datosProd = cursor.fetchone()  # Obtiene un solo registro
                nombreProd = datosProd['nombre']
                precioProd = datosProd['precio']
                # Obtener persona_id de Usuario
                sqlUsuario = "select persona_id from usuario where id=%s"
                cursor.execute(sqlUsuario, [self.usuario_id_registro])
                datosUsuario = cursor.fetchone()  # Obtiene un solo registro
                persona_id = datosUsuario['persona_id']
                # Obtener datos de Persona
                sqlPersona = "select apellido_paterno, apellido_materno,nombre from persona where id=%s"
                cursor.execute(sqlPersona, [persona_id])
                datosPersona = cursor.fetchone()  # Obtiene un solo registro
                nombrePersona = datosPersona['apellido_paterno'] + ' ' + \
                    datosPersona['apellido_materno'] + \
                    ' ' + datosPersona['nombre']

                # Insertar salida en kardex para almacén origen
                objKardex = Kardex(0, self.almacen_origen_id, nombreAlmacenO,
                                   det2["producto_id"], nombreProd, precioProd, cant_salida=det2["cantidad"],
                                   persona_id=persona_id, persona_nombre=nombrePersona,
                                   transaccion_id=self.id, nro_doc_transaccion="TrasSal1", tipo_movimiento_id=4, concepto=self.motivo)
                # Llamar al método insertar Kardex
                objKardex.insertar()

                # Insertar ingreso en kardex para almacén destino
                objKardex2 = Kardex(0, self.almacen_final_id, nombreAlmacenF,
                                    det2["producto_id"], nombreProd, precioProd, cant_ingreso=det2["cantidad"],
                                    persona_id=persona_id, persona_nombre=nombrePersona,
                                    transaccion_id=self.id, nro_doc_transaccion="TrasIng1", tipo_movimiento_id=3, concepto=self.motivo)
                # Llamar al método insertar Kardex
                objKardex2.insertar()

            # Confirmar los cambios en la BD
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado Satisfactoriamente"})

        except con.Error as error:
            # Si hay unerror en la setencia aqui se muestra
            # Revoca los cambios de la BD
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:  # Se ejecuta despues de TRY o despues de EXEPT
            cursor.close()
            con.close()

    def anular(self, usuario_id_anulacion, id):
        # Abrir la conexion a la BD
        con = db().open

        # Indicar que los cambios realizados en la BD no se confirman de manera automatica
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        sqltraslado = "select almacen_origen_id, almacen_final_id from traslado where id= %s"
        cursor.execute(sqltraslado, [id])
        datostraslado = cursor.fetchone()  # Obtiene un solo registro

        almacen_origen_id = datostraslado['almacen_origen_id']
        almacen_final_id = datostraslado['almacen_final_id']

        sqltrasladodetalle = "select producto_id, cantidad from traslado_detalle where traslado_id= %s"
        cursor.execute(sqltrasladodetalle, [id])
        datostrasladodetalle = cursor.fetchall()  # jala todos los registros

        try:

            for datos in datostrasladodetalle:
                cantidad = datos["cantidad"]
                producto_id = datos['producto_id']
                # Aumentar cantidad de producto a almacen origen
                sql = "update stock_almacen set stock = stock + %s where almacen_id = %s and producto_id = %s"
                # Ejecutar el comando SQL (tabla stock_almacen)
                cursor.execute(sql, [cantidad, almacen_origen_id, producto_id])

                # restar cantidad de producto a almacen destino
                sql = "update stock_almacen set stock = stock - %s where almacen_id = %s and producto_id = %s"
                # Ejecutar el comando SQL (tabla stock_almacen)
                cursor.execute(sql, [cantidad, almacen_final_id, producto_id])

            # Preparar la sentencia SQL
            sql = "update traslado set estado='0', usuario_id_anulacion=%s, fecha_hora_anulacion=NOW() where id=%s"
            cursor.execute(sql, [usuario_id_anulacion, id])

            # Dar baja en kardex a traslado anulado
            sql = "update kardex set estado='0' where transaccion_id=%s and tipo_movimiento_id=3"
            cursor.execute(sql, [id])
            sql = "update kardex set estado='0' where transaccion_id=%s and tipo_movimiento_id=4"
            # Ejecutar el comando SQL (tabla kardex)
            cursor.execute(sql, [id])

            # Confirmar los cambios en la BD
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Anulado Satisfactoriamente"})

        except con.Error as error:
            # Si hay unerror en la setencia aqui se muestra
            # Revoca los cambios de la BD
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:  # Se ejecuta despues de TRY o despues de EXEPT
            cursor.close()
            con.close()
