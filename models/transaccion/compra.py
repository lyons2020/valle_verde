from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
from models.mantenimiento.kardex import Kardex
import json


class Compra():
    #*************Leo Chimoy*******************#
    def __init__(self, id=None, proveedor_id=None, tipo_comprobante_id=None, nser=None, ndoc=None, fdoc=None, sub_total=None, igv=None, total=None, porcentaje_igv=None, usuario_id_registro=None, almacen_id=None, detalle=None):
        self.id = id
        self.proveedor_id = proveedor_id
        self.tipo_comprobante_id = tipo_comprobante_id
        self.nser = nser
        self.ndoc = ndoc
        self.fdoc = fdoc
        self.sub_total = sub_total
        self.igv = igv
        self.total = total
        self.porcentaje_igv = porcentaje_igv
        self.usuario_id_registro = usuario_id_registro
        self.almacen_id = almacen_id
        self.detalle = detalle

    def listar(self):
        cursor = db().open.cursor()
        cursor.execute('select * from vista_compra_lista')
        datos = cursor.fetchall()  # jala todos los registros
        cursor.close()
        if datos:
            # cls=CustomJsonEncoder cuando encuentra un decimal lo convierte a float
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def insertar(self):
        # Abrir la conexion a la BD
        con = db().open

        # Indicar que los cambios realizados en la BD no se confirman de manera automatica
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Preparar la sentencia SQL insert (tabla compra)
        sql = "insert into compra (proveedor_id,tipo_comprobante_id,nser,ndoc,fdoc,sub_total,igv,total,porcentaje_igv,usuario_id_registro,almacen_id) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [self.proveedor_id, self.tipo_comprobante_id, self.nser, self.ndoc, self.fdoc,
                           self.sub_total, self.igv, self.total, self.porcentaje_igv, self.usuario_id_registro, self.almacen_id])

            # obtener el id de la compra para insertarla en la tabla compra_detalle
            self.id = con.insert_id()

            # Grabar en la tabla:compra_detalle
            # Recoger el JSON del detalle de la compra y colocarlo como JSON Array
            jsonArraycompraDetalle = json.loads(self.detalle)

            # recorrer los elementos del Array jsonArraycompraDetalle
            for det in jsonArraycompraDetalle:
                # Preparar la sentencia SQL insert (tabla compra_detalle)
                sql = "insert into compra_detalle (compra_id,producto_id,cantidad,precio) values(%s,%s,%s,%s)"
                # Ejecutar el comando SQL
                cursor.execute(
                    sql, [self.id, det["producto_id"], det["cantidad"], det["precio"]])

            # actualizar el stock de los productos segun la cantidad comprada
            # Obtener nombre de almacen
            sqlAlmacen = "select nombre from almacen where id=%s"
            cursor.execute(sqlAlmacen, [self.almacen_id])
            datosAlmacen = cursor.fetchone()  # Obtiene un solo registro
            nombreAlmacen = datosAlmacen['nombre']
            # Obtener nro-doc-compra para insertar en Kardex
            nroDocCompra = self.nser + '-' + self.ndoc
            for det2 in jsonArraycompraDetalle:
                # Buscar producto por almacén
                sqlProdAlm = "select * from stock_almacen where almacen_id=%s and  producto_id = %s"
                cursor.execute(
                    sqlProdAlm, [self.almacen_id, det2["producto_id"]])
                datostraslado = cursor.fetchone()  # Obtiene un solo registro
                sql = ""
                if datostraslado:
                    sql = "update stock_almacen set stock = stock + %s where almacen_id = %s and producto_id = %s"
                else:
                    sql = "insert into stock_almacen (stock,almacen_id,producto_id) values (%s,%s,%s)"

                cursor.execute(
                    sql, [det2["cantidad"], self.almacen_id, det2["producto_id"]])

                # Insertar en kardex el ingreso de mercadería
                # Obtener nombre de producto
                sqlProducto = "select nombre from producto where id=%s"
                cursor.execute(sqlProducto, [det2["producto_id"]])
                datosProd = cursor.fetchone()  # Obtiene un solo registro
                nombreProd = datosProd['nombre']
                # Obtener persona_id de Usuario
                sqlUsuario = "select persona_id from usuario where id=%s"
                cursor.execute(sqlUsuario, [self.usuario_id_registro])
                datosUsuario = cursor.fetchone()  # Obtiene un solo registro
                persona_id = datosUsuario['persona_id']
                # Obtener datos de Persona
                sqlPersona = "select apellido_paterno, apellido_materno,nombre from persona where id=%s"
                cursor.execute(sqlPersona, [persona_id])
                datosPersona = cursor.fetchone()  # Obtiene un solo registro
                nombrePersona = datosPersona['apellido_paterno'] + ' ' + \
                    datosPersona['apellido_materno'] + \
                    ' ' + datosPersona['nombre']

                objKardex = Kardex(0, self.almacen_id, nombreAlmacen,
                                   det2["producto_id"], nombreProd, det2["precio"], det2["cantidad"],
                                   persona_id=persona_id, persona_nombre=nombrePersona,
                                   transaccion_id=self.id, nro_doc_transaccion=nroDocCompra, tipo_movimiento_id=1)
                # Llamar al método insertar Kardex
                objKardex.insertar()

                # actualizar precio de compra en tabla producto (precio_inicial)
                sql = "update producto set precio_inicial = %s where id = %s"
                # Ejecutar el comando SQL (tabla stock_almacen)
                cursor.execute(sql, [det2["precio"], det2["producto_id"]])

            # Confirmar los cambios en la BD
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado Satisfactoriamente"})

        except con.Error as error:
            # Si hay unerror en la setencia aqui se muestra
            # Revoca los cambios de la BD
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:  # Se ejecuta despues de TRY o despues de EXEPT
            cursor.close()
            con.close()

    def anular(self, usuario_id_anulacion, id):
        # Abrir la conexion a la BD
        con = db().open

        # Indicar que los cambios realizados en la BD no se confirman de manera automatica
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Preparar la sentencia SQL
        sql = "update compra set estado='0', usuario_id_anulacion=%s, fecha_hora_anulacion=NOW() where id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [usuario_id_anulacion, id])

            sqlCompra = "select almacen_id from compra where id= %s"
            cursor.execute(sqlCompra, [id])
            datosCompra = cursor.fetchone()  # Obtiene un solo registro
            almacen_id = datosCompra['almacen_id']

            sqlCompraDet = "select producto_id, cantidad from compra_detalle where compra_id= %s"
            cursor.execute(sqlCompraDet, [id])

            datosDetalleCompra = cursor.fetchall()  # jala todos los registros

            for datos in datosDetalleCompra:
                cantidad = datos["cantidad"]
                producto_id = datos['producto_id']
                # restar cantidad de producto a almacen
                sql = "update stock_almacen set stock = stock - %s where almacen_id = %s and producto_id = %s"
                # Ejecutar el comando SQL (tabla stock_almacen)
                cursor.execute(sql, [cantidad, almacen_id, producto_id])

            # Dar baja en kardex a compra anulada
            sql = "update kardex set estado='0' where transaccion_id=%s and tipo_movimiento_id=1"
            # Ejecutar el comando SQL (tabla kardex)
            cursor.execute(sql, [id])

            # Confirmar los cambios en la BD
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Compra Anulada Satisfactoriamente"})

        except con.Error as error:
            # Si hay unerror en la setencia aqui se muestra
            # Revoca los cambios de la BD
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:  # Se ejecuta despues de TRY o despues de EXEPT
            cursor.close()
            con.close()
