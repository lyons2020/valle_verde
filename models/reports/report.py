from connection import Connection as db
from JsonEncoder import CustomJsonEncoder
import json


class Kardex():
    def __init__(self, id=None, almacen_id=None, almacen=None, producto_id=None,
                 producto=None, precio=None, cant_ingreso=None, cant_salida=None, fecha=None,
                 persona_id=None, persona_nombre=None, transaccion_id=None,
                 nro_doc_transaccion=None, tipo_movimiento_id=None, concepto=None,
                 estado=None
                 ):
        self.id = id
        self.almacen_id = almacen_id
        self.almacen = almacen
        self.producto_id = producto_id
        self.producto = producto
        self.precio = precio
        self.cant_ingreso = cant_ingreso
        self.cant_salida = cant_salida
        self.fecha = fecha
        self.persona_id = persona_id
        self.persona_nombre = persona_nombre
        self.transaccion_id = transaccion_id
        self.nro_doc_transaccion = nro_doc_transaccion
        self.tipo_movimiento_id = tipo_movimiento_id
        self.concepto = concepto
        self.estado = estado

    def insertar(self):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert (Tabla:venta)
        sql = "insert into kardex (almacen_id,almacen,producto_id,producto,precio,cant_ingreso,cant_salida,persona_id,persona_nombre,transaccion_id,nro_doc_transaccion,tipo_movimiento_id,concepto) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        try:
            # Ejecutar el comando SQL (Tabla:kardex)
            cursor.execute(sql, [self.almacen_id, self.almacen, self.producto_id, self.producto, self.precio,
                           self.cant_ingreso, self.cant_salida, self.persona_id, self.persona_nombre, self.transaccion_id, self.nro_doc_transaccion, self.tipo_movimiento_id, self.concepto])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Grabado satisfactoriamente"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

    def anular(self, tipo_movimiento_id, transaccion_id):
        # Abrir conexión a la base de datos
        con = db().open

        # Indicar que los cambios realizados en la base de datos no se confirman de manera automática
        con.autocommit = False

        # Crear un cursor
        cursor = con.cursor()

        # Prepapar la sentencia SQL Insert
        sql = "update kardex set estado='0' where tipo_movimiento_id=%s and transaccion_id=%s"

        try:
            # Ejecutar el comando SQL
            cursor.execute(sql, [tipo_movimiento_id, transaccion_id])

            # Confirmar los cambios en la base datos
            con.commit()

            # Retorna un mensaje satisfactorio
            return json.dumps({"status": True, "data": "Registro anulado satisfactoriamente en kardex"})

        except con.Error as error:
            # Revoca los cambios de la base de datos
            con.rollback()

            # Retorna un mensaje de error
            return json.dumps({"status": False, "data": format(error)}, cls=CustomJsonEncoder)

        finally:
            cursor.close()
            con.close()

# *******Reports*******
    # Por almacén, fecha inicio y fecha fin
    def search_kardex(self, almacen_id, producto_id, date_ini, date_fin):
        cursor = db().open.cursor()
        cursor.callproc(
            'getKardex', [almacen_id, producto_id, date_ini, date_fin])
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})

    def search_details(self, almacen_id, producto_id):
        cursor = db().open.cursor()
        cursor.execute('SELECT k.id,k.fecha,k.persona_id,k.persona_nombre,tm.tipo AS tipo_movimiento,k.transaccion_id,k.nro_doc_transaccion,(case when ISNULL(k.ingreso) then 0 ELSE k.ingreso END) AS ingreso,(case when ISNULL(k.salida) then 0 ELSE k.salida END) AS salida,k.estado FROM kardex k INNER JOIN tipo_movimiento tm ON(tm.id=k.tipo_movimiento_id) WHERE almacen_id=%s AND producto_id=%s ORDER BY fecha', [
                       almacen_id, producto_id])
        # Capturar los datos
        datos = cursor.fetchall()
        cursor.close()
        if datos:
            return json.dumps({"status": True, "data": datos}, cls=CustomJsonEncoder)
        else:
            return json.dumps({"status": False, "data": "No hay datos"})
