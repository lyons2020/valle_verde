from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.sistema.menu_perfil import MenuPerfil
from models.sistema.menu import Menu
from models.sistema.perfil import Perfil
# Validations
from validations.sistema.menu_perfil import StoreMenuPerfilRequest
import json

view_menu_perfil = Blueprint('view_menu_perfil', __name__,
                             template_folder='templates', static_folder='static')


@view_menu_perfil.route('/menuperfiles')
def menuperfiles():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        objP = MenuPerfil()
        rptaJSON = objP.listar()
        datosP = json.loads(rptaJSON)
        return render_template('sistema/menu_perfil/index.html', datos=session, menuperfiles=datosP)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_menu_perfil.route('/menuperfiles/add', methods=('GET', 'POST'))
def menuperfiles_add():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreMenuPerfilRequest()  # Validations/mantenimiento/proveedor.py
        if form.validate_on_submit():  # Si es POST y  pasa las validaciones del formulario
            return menuperfiles_save()  # Grabar
        # Sino es POST
        objM = Menu()
        rptaJSON = objM.listar()
        datos_menu = json.loads(rptaJSON)
        objP = Perfil()
        rptaJSON = objP.listar()
        datos_perfil = json.loads(rptaJSON)

        return render_template('sistema/menu_perfil/add-edit.html', datos=session, menus=datos_menu, perfiles=datos_perfil, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


# @view_menu_perfil.route('/menuperfiles/save', methods=['POST'])
def menuperfiles_save():
    if "nombre" in session:  # Significa que el menu si ha iniciado sesión
        if request.method == 'POST':
            # Captura de datos del formulario
            id = request.form['id']
            menu_id = request.form['menu']
            perfil_id = request.form['perfil']
            # Instancia el objeto de la clase UsuarioPerfil y ejecuta los métodos
            objMP = MenuPerfil(id, menu_id, perfil_id)
            if id == '0':
                # Llamar al método insertar
                rptaJSON = objMP.insertar()
            else:
                # Llamar al método editar
                rptaJSON = objMP.editar()

            # Convertir el resultado JSON String JSON Array
            datosP = json.loads(rptaJSON)

            # Imprimir el mensaje en pantalla
            flash(datosP['data'])

            # Llamar a la rura '/colaboradores'
            return redirect(url_for('view_menu_perfil.menuperfiles'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_menu_perfil.route('/menuperfiles/edit/<id>', methods=('GET', 'POST'))
def menuperfiles_edit(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreMenuPerfilRequest()
        if form.validate_on_submit():
            return menuperfiles_save()
        # Obtiene los datos del Usuario Perfil a editar
        objUMP = MenuPerfil()
        rptaJSON = objUMP.leer(id)
        datos_menu_perfil = json.loads(rptaJSON)
        objM = Menu()
        rptaJSON = objM.listar()
        datos_menu = json.loads(rptaJSON)
        objP = Perfil()
        rptaJSON = objP.listar()
        datos_perfil = json.loads(rptaJSON)

        return render_template('sistema/menu_perfil/add-edit.html', datos=session, menuperfiles=datos_menu_perfil, menus=datos_menu, perfiles=datos_perfil, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_menu_perfil.route('/menuperfiles/delete/<id>')
def menuperfiles_delete(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión

        # Instanciar la clase menu
        objUMP = MenuPerfil()
        rptaJSON = objUMP.eliminar(id)
        datos_menu_perfil = json.loads(rptaJSON)

        # Imprimir el mensaje en pantalla
        flash(datos_menu_perfil['data'])

        # Llamar a la ruta '/menuperfiles'
        return redirect(url_for('view_menu_perfil.menuperfiles'))
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
