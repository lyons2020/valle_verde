from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.sistema.perfil import Perfil
from models.sistema.menu_perfil import MenuPerfil
from validations.sistema.perfil import StorePerfilRequest
import json

view_perfil = Blueprint('view_perfil', __name__,
                        template_folder='templates', static_folder='static')


@view_perfil.route('/perfiles')
def perfiles():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        objP = Perfil()
        rptaJSON = objP.listar()
        datosP = json.loads(rptaJSON)
        return render_template('sistema/perfil/index.html', datos=session, perfiles=datosP)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_perfil.route('/perfiles/add', methods=('GET', 'POST'))
def perfiles_add():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StorePerfilRequest()  # Validations/mantenimiento/proveedor.py
        if form.validate_on_submit():  # Si es POST y  pasa las validaciones del formulario
            return perfiles_save()  # Grabar
        return render_template('sistema/perfil/add-edit.html', datos=session, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


# @view_perfil.route('/perfiles/save', methods=['POST'])
def perfiles_save():
    if "nombre" in session:  # Significa que el menu si ha iniciado sesión
        if request.method == 'POST':
            # Captura de datos del formulario
            id = request.form['id']
            nombre = request.form['nombre']

            # Instancia el objeto de la clase Persona y ejecuta los métodos
            objP = Perfil(id, nombre)
            if id == '0':
                # Llamar al método insertar
                rptaJSON = objP.insertar()
            else:
                # Llamar al método editar
                rptaJSON = objP.editar()

            # Convertir el resultado JSON String JSON Array
            datosP = json.loads(rptaJSON)

            # Imprimir el mensaje en pantalla
            flash(datosP['data'])

            # Llamar a la rura '/colaboradores'
            return redirect(url_for('view_perfil.perfiles'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_perfil.route('/perfiles/edit/<id>', methods=('GET', 'POST'))
def perfiles_edit(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StorePerfilRequest()  # Validations/mantenimiento/proveedor.py
        if form.validate_on_submit():  # Si es POST y  pasa las validaciones del formulario
            return perfiles_save()  # Grabar
        # Obtiene los datos del producto a editar
        objP = Perfil()
        rptaJSON = objP.leer(id)
        datos_perfil = json.loads(rptaJSON)

        return render_template('sistema/perfil/add-edit.html', datos=session, perfiles=datos_perfil, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_perfil.route('/perfiles/delete/<id>')
def perfiles_delete(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        # Instanciar la clase menu
        objP = Perfil()
        # Verificar si usuario tiene perfiles asociados
        objMenuP = MenuPerfil()
        rptaJSON = objMenuP.listar_menu_x_perfil(id)
        datos = json.loads(rptaJSON)
        if(datos['status']):  # Perfiles tiene menu
            # Imprimir el mensaje en pantalla
            flash("No se puede eliminar Perfil ya que tiene menus asociados.")
            # Llamar a la ruta '/colaboradores'
            return redirect(url_for('view_perfil.perfiles'))
        else:
            # Verificar si perfil está asignado a usuarios
            rptaJSON = objP.listar_usuario_x_perfil(id)
            datos = json.loads(rptaJSON)
            if(datos['status']):  # Usuario tiene perfiles
                # Imprimir el mensaje en pantalla
                flash("Perfil tiene usuarios asociados (Usuario Perfil)")
                # Llamar a la ruta '/colaboradores'
                return redirect(url_for('view_perfil.perfiles'))
            else:
                rptaJSON = objP.eliminar(id)
                datos_perfil = json.loads(rptaJSON)

                # Imprimir el mensaje en pantalla
                flash(datos_perfil['data'])

                # Llamar a la ruta '/perfiles'
                return redirect(url_for('view_perfil.perfiles'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
