from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.sistema.menu import Menu
# Validations
from validations.sistema.menu import StoreMenuRequest
import json

view_menu = Blueprint('view_menu', __name__,
                      template_folder='templates', static_folder='static')


@view_menu.route('/menus')
def menus():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        objM = Menu()
        rptaJSON = objM.listar()
        datosMenu = json.loads(rptaJSON)
        return render_template('sistema/menu/index.html', datos=session, menus=datosMenu)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_menu.route('/menus/add', methods=('GET', 'POST'))
def menus_add():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreMenuRequest()  # Validations/mantenimiento/proveedor.py
        if form.validate_on_submit():  # Si es POST y  pasa las validaciones del formulario
            return menus_save()  # Grabar

        # Sino es POST
        objM = Menu()
        rptaJSON = objM.listar()
        datosMenuPadre = json.loads(rptaJSON)
        return render_template('sistema/menu/add-edit.html', datos=session, menusPadre=datosMenuPadre, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


# @view_menu.route('/menus/save', methods=['POST'])
def menus_save():
    if "nombre" in session:  # Significa que el menu si ha iniciado sesión
        if request.method == 'POST':
            # Captura de datos del formulario
            id = request.form['id']
            nombre = request.form['nombre']
            route = request.form['route']
            menu_id = request.form['menupadre'] or None

            # Instancia el objeto de la clase Menu y ejecuta los métodos
            objM = Menu(id, menu_id, nombre, route)
            if id == '0':
                # Llamar al método insertar
                rptaJSON = objM.insertar()
            else:
                # Llamar al método editar
                rptaJSON = objM.editar()

            # Convertir el resultado JSON String JSON Array
            datosM = json.loads(rptaJSON)

            # Imprimir el mensaje en pantalla
            flash(datosM['data'])

            # Llamar a la ruta '/colaboradores'
            return redirect(url_for('view_menu.menus'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_menu.route('/menus/edit/<id>', methods=('GET', 'POST'))
def menus_edit(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreMenuRequest()
        if form.validate_on_submit():
            return menus_save()
        # Obtiene los datos del menu a editar
        objM = Menu()
        rptaJSON = objM.leer(id)
        datos_menu = json.loads(rptaJSON)

        rptaJSON = objM.listar()
        datosMenuPadre = json.loads(rptaJSON)

        return render_template('sistema/menu/add-edit.html', datos=session, menus=datos_menu, menusPadre=datosMenuPadre, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_menu.route('/menus/delete/<id>')
def menus_delete(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión

        # Instanciar la clase menu
        objM = Menu()
        rptaJSON = objM.eliminar(id)
        datos_menu = json.loads(rptaJSON)

        # Imprimir el mensaje en pantalla
        flash(datos_menu['data'])

        # Llamar a la ruta '/menus'
        return redirect(url_for('view_menu.menus'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
