from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.sistema.usuario_perfil import UsuarioPerfil
from models.sistema.perfil import Perfil
from models.usuario import Usuario
# Validations
from validations.sistema.usuario_perfil import StoreUsuarioPerfilRequest
import json

view_usuario_perfil = Blueprint('view_usuario_perfil', __name__,
                                template_folder='templates', static_folder='static')


@view_usuario_perfil.route('/usuarioperfiles')
def usuarioperfiles():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        objP = UsuarioPerfil()
        rptaJSON = objP.listar()
        datosP = json.loads(rptaJSON)
        return render_template('sistema/usuario_perfil/index.html', datos=session, usuarioperfiles=datosP)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_usuario_perfil.route('/usuarioperfiles/add', methods=('GET', 'POST'))
def usuarioperfiles_add():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreUsuarioPerfilRequest()  # Validations/mantenimiento/proveedor.py
        if form.validate_on_submit():  # Si es POST y  pasa las validaciones del formulario
            return usuarioperfiles_save()  # Grabar
        # Sino es POST
        objU = Usuario()
        rptaJSON = objU.listar()
        datos_usuario = json.loads(rptaJSON)
        objP = Perfil()
        rptaJSON = objP.listar()
        datos_perfil = json.loads(rptaJSON)
        # Sino es POST
        return render_template('sistema/usuario_perfil/add-edit.html', datos=session, usuarios=datos_usuario, perfiles=datos_perfil, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


# @view_usuario_perfil.route('/usuarioperfiles/save', methods=['POST'])
def usuarioperfiles_save():
    if "nombre" in session:  # Significa que el menu si ha iniciado sesión
        if request.method == 'POST':
            # Captura de datos del formulario
            id = request.form['id']
            perfil_id = request.form['perfil']
            usuario_id = request.form['usuario']
            # Instancia el objeto de la clase UsuarioPerfil y ejecuta los métodos
            objP = UsuarioPerfil(id, perfil_id, usuario_id)
            if id == '0':
                # Llamar al método insertar
                rptaJSON = objP.insertar()
            else:
                # Llamar al método editar
                rptaJSON = objP.editar()

            # Convertir el resultado JSON String JSON Array
            datosP = json.loads(rptaJSON)

            # Imprimir el mensaje en pantalla
            flash(datosP['data'])

            # Llamar a la rura '/colaboradores'
            return redirect(url_for('view_usuario_perfil.usuarioperfiles'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_usuario_perfil.route('/usuarioperfiles/edit/<id>', methods=('GET', 'POST'))
def usuarioperfiles_edit(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreUsuarioPerfilRequest()
        if form.validate_on_submit():
            return usuarioperfiles_save()
        # Obtiene los datos del Usuario Perfil a editar
        objUP = UsuarioPerfil()
        rptaJSON = objUP.leer(id)
        datos_usuario_perfil = json.loads(rptaJSON)
        objU = Usuario()
        rptaJSON = objU.listar()
        datos_usuario = json.loads(rptaJSON)
        objP = Perfil()
        rptaJSON = objP.listar()
        datos_perfil = json.loads(rptaJSON)
        return render_template('sistema/usuario_perfil/add-edit.html', datos=session, usuarioperfiles=datos_usuario_perfil, usuarios=datos_usuario, perfiles=datos_perfil, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_usuario_perfil.route('/usuarioperfiles/delete/<id>')
def usuarioperfiles_delete(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión

        # Instanciar la clase menu
        objUP = UsuarioPerfil()
        rptaJSON = objUP.eliminar(id)
        datos_usuario_perfil = json.loads(rptaJSON)

        # Imprimir el mensaje en pantalla
        flash(datos_usuario_perfil['data'])

        # Llamar a la ruta '/usuarioperfiles'
        return redirect(url_for('view_usuario_perfil.usuarioperfiles'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
