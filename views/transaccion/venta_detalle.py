from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.transaccion.venta_detalle import VentaDetalle
import json

view_venta_detalle = Blueprint('view_venta_detalle', __name__,
                               template_folder='templates', static_folder='static')


@view_venta_detalle.route('/venta_detalles/venta/<id>')
def venta_detalles_buscar(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        objDV = VentaDetalle()
        # Buscar Proveedor en tabla persona ya que hija de tabla persona
        rptaJSON = objDV.search_details(id)

        return json.loads(rptaJSON)

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
