from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.mantenimiento.producto import Producto
#from models.categoria import Categoria
from models.transaccion.venta import Venta
from models.mantenimiento.tipocomprobante import TipoComprobante
from models.mantenimiento.serie import Serie
from models.mantenimiento.cliente import Cliente
from models.mantenimiento.configuracion import Configuracion
from models.mantenimiento.almacen import Almacen
from models.transaccion.traslado import Traslado
import json
from datetime import date
# CREAR VARIABLE view_venta
view_traslado = Blueprint('view_traslado', __name__,
                          template_folder='templates', static_folder='static')


@view_traslado.route('/traslados')
def traslados():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion
        objtraslado = Traslado()
        rptaJson = objtraslado.listar()
        datosTraslado = json.loads(rptaJson)
        return render_template('transaccion/traslado/traslado-listar.html', datos=session, traslados=datosTraslado)

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_traslado.route('/traslados/add')
def traslados_add():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        # Obtener fecha actual del servidor web
        today = date.today()
        ftraslado = today.strftime("%Y-%m-%d")  # 2021-05-03

        # Obtiene la lista de productos
        objProducto = Producto()
        rptaJSON = objProducto.listarProductoTransaccion()
        datos_prod = json.loads(rptaJSON)

        # Obtiene la lista de Almacen
        objAlmacen = Almacen()
        rptaJSON = objAlmacen.listar()
        datos_alm = json.loads(rptaJSON)

        # ENVIAMOS LOS TIPO DE COMPROBANTES
        return render_template('transaccion/traslado/traslado-agregar.html', datos=session, fecha_traslado=ftraslado, productos=datos_prod, datos_alm=datos_alm)
    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_traslado.route('/traslados/save', methods=['POST'])
def traslados_save():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        if request.method == 'POST':
            # RECIBIR LAS DATOS DEL TRASLADO
            usuario_id_registro = session["id"]
            almacen_origen_id = request.form['cboalmorigen']
            almacen_final_id = request.form['cboalmfinal']
            motivo = request.form['txtmotivo']
            fecha = request.form['txtfecha']

            # recibir los datos del detalle de la venta (vienen en formato JSON)
            detalle = request.form['txtdetalletraslado']

            # Instanciar al objeto objventa de la clase Venta y enviar los datos a grabar
            objTras = Traslado(0, usuario_id_registro, almacen_origen_id,
                               almacen_final_id, motivo, fecha, detalle)
            rptaJSON = objTras.insertar()
            datos_tras = json.loads(rptaJSON)

            # imprimir el mensaje en pantalla
            flash(datos_tras["data"])

            # llamar a la ruta ventas
            return redirect(url_for('view_traslado.traslados'))

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_traslado.route('/traslados/cancel/<id>')
def traslados_cancel(id):
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        # Instanciar la clase venta
        objVenta = Traslado()
        rptaJSON = objVenta.anular(session["id"], id)
        datos_venta = json.loads(rptaJSON)

        # imprimir el mensaje en pantalla
        flash(datos_venta['data'])

        # Llamar a la ruta '/productos'
        return redirect(url_for('view_traslado.traslados'))

    else:  # no iniciado sesion
        return redirect(url_for('view_sesion.login'))
