from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.transaccion.venta import Venta
from models.mantenimiento.tipocomprobante import TipoComprobante
from models.mantenimiento.serie import Serie
from models.mantenimiento.cliente import Cliente
from models.mantenimiento.producto import Producto
from models.mantenimiento.configuracion import Configuracion
from models.mantenimiento.almacen import Almacen
# Validations
from validations.reports.venta import SearchVentaRequest
import json
from datetime import date

view_venta = Blueprint('view_venta', __name__,
                       template_folder='templates', static_folder='static')


@view_venta.route('/ventas')
def ventas():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion
        objVenta = Venta()
        rptaJson = objVenta.listar()
        datosVenta = json.loads(rptaJson)
        return render_template('transaccion/venta/venta-listar.html', datos=session, ventas=datosVenta)

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_venta.route('/ventas/add')
def ventas_add():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        # Obtiene la lista de tipos de comprobantes
        objTC = TipoComprobante()
        rptaJSON = objTC.listarTCVenta()
        datos_tc = json.loads(rptaJSON)

        # Obtener fecha actual del servidor web
        today = date.today()
        fvta = today.strftime("%Y-%m-%d")  # 2021-05-03

        # Obtiene la lista de clientes
        objCliente = Cliente()
        rptaJSON = objCliente.listarClienteVenta()
        datos_cli = json.loads(rptaJSON)

        # Obtiene la lista de productos
        objProducto = Producto()
        rptaJSON = objProducto.listarProductoTransaccion()
        datos_prod = json.loads(rptaJSON)

        # obtiene el porcentaje de IGV
        objConf = Configuracion()
        rptaJSON = objConf.obtenerValorConfiguracion('PORIGV')
        porc_igv = json.loads(rptaJSON)

        # Obtiene la lista de Almacen
        objAlmacen = Almacen()
        rptaJSON = objAlmacen.listar()
        datos_alm = json.loads(rptaJSON)

        # ENVIAMOS LOS TIPO DE COMPROBANTES
        return render_template('transaccion/venta/venta-agregar.html', datos=session, tipos_comprobante=datos_tc, fecha_venta=fvta, clientes=datos_cli, productos=datos_prod, porc_igv=porc_igv, datos_alm=datos_alm)
    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_venta.route('/ventas/get/serie', methods=['POST'])
def ventas_get_serie():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        if request.method == 'POST':
            # RECIBIR EL TIPO DE COMPROBANTE
            tc_id = request.form['tc_id']

            # Obtiene la lista de series segun el tipo de comprobate seleccionado
            objSerie = Serie()
            rptaJSON = objSerie.listarSerie(tc_id)
            datos_serie = json.loads(rptaJSON)
            return datos_serie

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_venta.route('/ventas/get/correlativo', methods=['POST'])
def ventas_get_correlativo():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        if request.method == 'POST':
            # RECIBIR LA SERIE
            serie_nro = request.form['serie_nro']

            # Obtiene el correlativo para la serie
            objSerie = Serie()
            rptaJSON = objSerie.correlativo(serie_nro)
            correlativo = json.loads(rptaJSON)
            return correlativo

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_venta.route('/ventas/save', methods=['POST'])
def ventas_save():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        if request.method == 'POST':
            # RECIBIR LAS DATOS DE LA VENTA
            cliente_id = request.form['cbocliente']
            tipo_comprobante_id = request.form['cbotc']
            nser = request.form['cboserie']
            ndoc = request.form['txtndoc']
            fdoc = request.form['txtfdoc']
            sub_total = request.form['txtsubtotal']
            igv = request.form['txtigv']
            total = request.form['txttotalneto']
            porcentaje_igv = request.form['txtporcentajeigv']
            usuario_id_registro = session["id"]
            almacen_id = request.form['cboalmacen']
            print('estoy en vista venta')
            print(usuario_id_registro)
            # recibir los datos del detalle de la venta (vienen en formato JSON)
            detalle = request.form['txtdetalleventa']

            # Instanciar al objeto objventa de la clase Venta y enviar los datos a grabar
            objVta = Venta(0, cliente_id, tipo_comprobante_id, nser, ndoc, fdoc, sub_total,
                           igv, total, porcentaje_igv, usuario_id_registro, almacen_id, detalle)
            rptaJSON = objVta.insertar()
            datos_venta = json.loads(rptaJSON)

            # imprimir el mensaje en pantalla
            flash(datos_venta["data"])

            # llamar a la ruta ventas
            return redirect(url_for('view_venta.ventas'))

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_venta.route('/ventas/cancel/<id>')
def ventas_cancel(id):
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        # Instanciar la clase venta
        objVenta = Venta()
        rptaJSON = objVenta.anular(session["id"], id)
        datos_venta = json.loads(rptaJSON)

        # imprimir el mensaje en pantalla
        flash(datos_venta['data'])

        # Llamar a la ruta '/ventas'
        return redirect(url_for('view_venta.ventas'))

    else:  # no iniciado sesion
        return redirect(url_for('view_sesion.login'))


# Reports Luis Azalde
@view_venta.route('/ventas/reporte', methods=['GET', 'POST'])
def ventas_reporte():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = SearchVentaRequest()
        datosV = []
        if form.validate_on_submit():
            print('di click en submit')
            almacen_id = form.almacen.data
            date_ini = form.date_ini.data
            date_fin = form.date_fin.data
            print('almacen_id: ' + almacen_id)
            print('date_ini: ' + str(date_ini))
            print('date_fin: ' + str(date_fin))
            objV = Venta()
            rptaJSON = objV.search_vta_date_alm(almacen_id, date_ini, date_fin)
            datosV = json.loads(rptaJSON)
            print(datosV)
        objA = Almacen()
        rptaJSON = objA.listar()
        datosA = json.loads(rptaJSON)
        return render_template('reports/ventas.html', datos=session, almacenes=datosA, ventas=datosV, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
