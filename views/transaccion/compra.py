from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.mantenimiento.producto import Producto
from models.transaccion.venta import Venta
from models.mantenimiento.tipocomprobante import TipoComprobante
from models.mantenimiento.serie import Serie
from models.mantenimiento.proveedor import Proveedor
from models.transaccion.compra import Compra
from models.mantenimiento.configuracion import Configuracion
from models.mantenimiento.almacen import Almacen
import json
from datetime import date
# CREAR VARIABLE view_compra
view_compra = Blueprint('view_compra', __name__,
                        template_folder='templates', static_folder='static')


@view_compra.route('/compras')
def compras():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion
        objCompra = Compra()
        rptaJson = objCompra.listar()
        datosCompra = json.loads(rptaJson)
        return render_template('transaccion/compra/compra-listar.html', datos=session, compras=datosCompra)

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_compra.route('/compras/add')
def compras_add():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        # Obtiene la lista de tipos de comprobantes
        objTC = TipoComprobante()
        rptaJSON = objTC.listarTCCompra()
        datos_tc = json.loads(rptaJSON)

        # Obtener fecha actual del servidor web
        today = date.today()
        fvta = today.strftime("%Y-%m-%d")  # 2021-05-03

        # Obtiene la lista de proveedores
        objProveedor = Proveedor()
        rptaJSON = objProveedor.listarProveedorCompra()
        datos_pro = json.loads(rptaJSON)

        # Obtiene la lista de productos
        objProducto = Producto()
        rptaJSON = objProducto.listarProductoTransaccion()
        datos_prod = json.loads(rptaJSON)

        # obtiene el porcentaje de IGV
        objConf = Configuracion()
        rptaJSON = objConf.obtenerValorConfiguracion('PORIGV')
        porc_igv = json.loads(rptaJSON)

        # Obtiene la lista de Almacen
        objAlmacen = Almacen()
        rptaJSON = objAlmacen.listar()
        datos_alm = json.loads(rptaJSON)

        # ENVIAMOS LOS TIPO DE COMPROBANTES
        return render_template('transaccion/compra/compra-agregar.html', datos=session, tipos_comprobante=datos_tc, fecha_venta=fvta, proveedores=datos_pro, productos=datos_prod, porc_igv=porc_igv, datos_alm=datos_alm)
    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_compra.route('/compras/get/serie', methods=['POST'])
def compras_get_serie():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        if request.method == 'POST':
            # RECIBIR EL TIPO DE COMPROBANTE
            tc_id = request.form['tc_id']

            # Obtiene la lista de series segun el tipo de comprobate seleccionado
            objSerie = Serie()
            rptaJSON = objSerie.listarSerie(tc_id)
            datos_serie = json.loads(rptaJSON)
            return datos_serie

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_compra.route('/compras/get/correlativo', methods=['POST'])
def compras_get_correlativo():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        if request.method == 'POST':
            # RECIBIR LA SERIE
            serie_nro = request.form['serie_nro']

            # Obtiene el correlativo para la serie
            objSerie = Serie()
            rptaJSON = objSerie.correlativo(serie_nro)
            correlativo = json.loads(rptaJSON)
            return correlativo

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_compra.route('/compras/save', methods=['POST'])
def compras_save():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        if request.method == 'POST':
            # RECIBIR LAS DATOS DE LA COMPRA
            proveedor_id = request.form['cboproveedor']
            tipo_comprobante_id = request.form['cbotc']
            nser = request.form['txtserie']
            ndoc = request.form['txtndoc']
            fdoc = request.form['txtfdoc']
            sub_total = request.form['txtsubtotal']
            igv = request.form['txtigv']
            total = request.form['txttotalneto']
            porcentaje_igv = request.form['txtporcentajeigv']
            usuario_id_registro = session["id"]
            almacen_id = request.form['cboalmacen']

            # recibir los datos del detalle de la compra (vienen en formato JSON)

            detalle = request.form['txtdetallecompra']

            # Instanciar al objeto objCompra de la clase Compra y enviar los datos a grabar
            objCompra = Compra(0, proveedor_id, tipo_comprobante_id, nser, ndoc, fdoc, sub_total,
                               igv, total, porcentaje_igv, usuario_id_registro, almacen_id, detalle)
            rptaJSON = objCompra.insertar()
            datos_compra = json.loads(rptaJSON)

            # imprimir el mensaje en pantalla
            flash(datos_compra["data"])

            # llamar a la ruta ventas
            return redirect(url_for('view_compra.compras'))

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_compra.route('/compras/cancel/<id>')
def compras_cancel(id):
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        # Instanciar la clase compra
        objCompra = Compra()
        rptaJSON = objCompra.anular(session["id"], id)
        datos_compra = json.loads(rptaJSON)

        # imprimir el mensaje en pantalla
        flash(datos_compra['data'])

        # Llamar a la ruta '/productos'
        return redirect(url_for('view_compra.compras'))

    else:  # no iniciado sesion
        return redirect(url_for('view_sesion.login'))
