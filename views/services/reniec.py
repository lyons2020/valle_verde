from flask import Blueprint,  session, redirect, url_for
from services.reniec import Reniec
import json

view_services_reniec = Blueprint('view_services_reniec', __name__,
                                 template_folder='templates', static_folder='static')


@view_services_reniec.route('/reniec/buscar/<dni>')
def reniec_buscar(dni):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        # Consumir servicio SUNAT
        objReniec = Reniec(dni)
        rptaJSON = objReniec.buscar_dni()
        return json.loads(rptaJSON)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
