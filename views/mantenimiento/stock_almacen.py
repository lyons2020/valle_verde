from flask import Blueprint, render_template, request, session, redirect, url_for, jsonify
from models.mantenimiento.stock_almacen import StockAlmacen
import json


view_stock_almacen = Blueprint('view_stock_almacen', __name__,
                               template_folder='templates', static_folder='static')


@view_stock_almacen.route('/stock_almacenes/productos/<almacen_id>')
def stock_almacenes_prod(almacen_id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión

        objP = StockAlmacen()
        rptaJSON = objP.listar_prods_almacen(almacen_id)
        return json.loads(rptaJSON)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_stock_almacen.route('/stock_almacen/get/data', methods=['POST'])
def stock_almacen_get_data():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        if request.method == 'POST':
            # RECIBIR EL PRODUCTO ID
            producto_id = request.form['producto_id']
            almacen_id = request.form['almacen_id']

            # Obtiene los datos del producto segun su id
            objStockAlmacen = StockAlmacen()
            rptaJSON = objStockAlmacen.leer(producto_id, almacen_id)
            datosStock = json.loads(rptaJSON)
            return datosStock

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))


@view_stock_almacen.route('/stock_almacen/get/producto', methods=['POST'])
def stock_almacen_get_producto():
    if "nombre" in session:  # significa que el usuario si ha iniciado sesion

        if request.method == 'POST':
            # RECIBIR EL ALMACEN ID
            almacen_id = request.form['almacen_id']

            # Obtiene los datos del producto segun su id
            objStockAlmacen = StockAlmacen()
            rptaJSON = objStockAlmacen.prodalm(almacen_id)
            datosStock = json.loads(rptaJSON)
            # Imprimir json rpta
            print(jsonify(datosStock), 200)
            return datosStock

    else:  # no ha iniciado sesion
        return redirect(url_for('view_sesion.login'))
