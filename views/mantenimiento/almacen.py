from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.mantenimiento.almacen import Almacen
# from models.mantenimiento.departamento import Departamento
# from models.mantenimiento.provincia import Provincia
# from models.mantenimiento.distrito import Distrito

import json

view_almacen = Blueprint('view_almacen', __name__,
                         template_folder='templates', static_folder='static')


@view_almacen.route('/almacenes')  # INDEX
def almacenes():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        objA = Almacen()
        rptaJSON = objA.listar()
        datosA = json.loads(rptaJSON)
        return render_template('mantenimiento/almacen/index.html', datos=session, almacenes=datosA)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_almacen.route('/almacenes/add')
def almacenes_add():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        return render_template('mantenimiento/almacen/almacenes-agregar-editar.html', datos=session)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_almacen.route('/almacenes/save', methods=['POST'])  # GUARDAR
def almacenes_save():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        if request.method == 'POST':
            # Captura de datos del formulario
            id = request.form['txtid']
            nombre = request.form['txtnombre']
            direccion = request.form['txtdireccion']

            # Instancia el objeto de la clase almacen y ejecuta los métodos
            objA = Almacen(id, nombre, direccion)

            if id == '0':
                # Llamar al método insertar
                rptaJSON = objA.insertar()
            else:
                # Llamar al método editar
                rptaJSON = objA.editar()

            # Convertir el resultado JSON String JSON Array
            datosA = json.loads(rptaJSON)

            # Imprimir el mensaje en pantalla
            flash(datosA['data'])

            # Llamar a la ruta '/almacenes'
            return redirect(url_for('view_almacen.almacenes'))

        else:  # No ha inciado sesión
            return redirect(url_for('view_sesion.login'))


@view_almacen.route('/almacenes/edit/<id>')
def almacenes_edit(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión

        # Obtiene los datos del producto a editar
        objA = Almacen()
        rptaJSON = objA.leer(id)
        datos_al = json.loads(rptaJSON)

        return render_template('mantenimiento/almacen/almacenes-agregar-editar.html', datos=session, almacenes=datos_al)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_almacen.route('/almacenes/delete/<id>')
def almacenes_delete(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión

        # Instanciar la clase producto
        objA = Almacen()
        rptaJSON = objA.eliminar(id)
        datos_A = json.loads(rptaJSON)

        # Imprimir el mensaje en pantalla
        flash(datos_A['data'])

        # Llamar a la rura '/almacenes'
        return redirect(url_for('view_almacen.almacenes'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
