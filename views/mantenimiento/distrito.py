from flask import Blueprint, request, session, redirect, url_for, flash
from models.mantenimiento.distrito import Distrito
import json

view_distrito = Blueprint('view_distrito', __name__,
                          template_folder='templates', static_folder='static')


@view_distrito.route('/distritos/prov/<id>')
def distritos_prov(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        # Obtiene la lista de DPTO para el combo cmbDpto
        objDist = Distrito()
        rptaJSON = objDist.listar_dist_prov(id)
        datos_distri = json.loads(rptaJSON)
        return datos_distri
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
