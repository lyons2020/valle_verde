from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.mantenimiento.persona import Persona
from models.mantenimiento.cliente import Cliente
from models.mantenimiento.departamento import Departamento
from models.mantenimiento.provincia import Provincia
from models.mantenimiento.distrito import Distrito
from models.mantenimiento.tipodocumento import TipoDocumento
from models.mantenimiento.cliente import Cliente
from services.reniec import Reniec
from services.sunat import Sunat
# Validations
from validations.mantenimiento.cliente import StoreClienteRequest
import json

view_cliente = Blueprint('view_cliente', __name__,
                         template_folder='templates', static_folder='static')


# esta ruta se invoca desde la pagina layout.html (nav->ul)
@view_cliente.route('/clientes')
def clientes():  # Listar en tabla proveedores HTML
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        objC = Cliente()
        rptaJSON = objC.listar()
        datosC = json.loads(rptaJSON)
        return render_template('mantenimiento/cliente/index.html', datos=session, clientes=datosC)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_cliente.route('/clientes/add', methods=('GET', 'POST'))
def clientes_add():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreClienteRequest()  # Validations/mantenimiento/cliente.py
        if form.validate_on_submit():  # Si es POST y  pasa las validaciones del formulario
            return clientes_save()  # Grabar
        # Sino es POST
        # Obtiene lista de DPTO para el combo cmbDpto
        objDpto = Departamento()
        rptaJSON = objDpto.listar()
        datos_dpto = json.loads(rptaJSON)
        # Obtiene lista de Tipo Docs para el combo tipodoc
        objTdoc = TipoDocumento()
        rptaJSON = objTdoc.listar()
        datos_tipo_doc = json.loads(rptaJSON)
        return render_template('mantenimiento/cliente/add-edit.html', datos=session, dptos=datos_dpto, tipo_docs=datos_tipo_doc, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


# @view_cliente.route('/clientes/save', methods=['POST'])
def clientes_save():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        if request.method == 'POST':
            # Captura de datos del formulario
            # Datos para tabla persona
            dni = request.form['numero_doc']
            paterno = request.form['paterno']
            materno = request.form['materno']
            nombre = request.form['nombre']
            direccion = request.form['direccion']
            referencia = request.form['referencia']
            email = request.form['email']
            celular = request.form['celular']
            distrito_id = request.form['cmbdist']
            # Datos para tabla usuario
            id = request.form['id']  # Id de cliente

            if id == '0':
                tipodoc = request.form['tipodoc']
                # Insertar en tabla persona
                oPersona = Persona(id, dni, paterno, materno, nombre, direccion, referencia,
                                   email, tipodoc, celular, distrito_id, 3)

                # Llamar al método insertar Persona
                rptaJSON = oPersona.insertar()
                # Llamar al método insertar Proveedor
                persona_id = json.loads(rptaJSON)['data']
                oCliente = Cliente(id, persona_id=persona_id)
                rptaJSON = oCliente.insertar()
            else:
                # Obtener persona_id de la tabla usuario con el id usuario
                oCliente = Cliente()
                rptaJSON = oCliente.buscar_id(id)
                persona_id = json.loads(rptaJSON)['data']['persona_id']
                # Actualizar en tabla Persona
                oPersona = Persona(persona_id, apellido_paterno=paterno, apellido_materno=materno, nombre=nombre,
                                   direccion=direccion, referencia=referencia, email=email, celular=celular, distrito_id=distrito_id)
                rptaJSON = oPersona.editar()

            # Convertir el resultado JSON String JSON Array
            datosP = json.loads(rptaJSON)
            # Imprimir el mensaje en pantalla
            flash(datosP['data'])

            # Llamar a la ruta '/clientes'
            return redirect(url_for('view_cliente.clientes'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_cliente.route('/clientes/edit/<id>', methods=('GET', 'POST'))
def clientes_edit(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreClienteRequest()
        if form.validate_on_submit():
            return clientes_save()
        # Obtiene los datos del cliente a editar
        objC = Cliente()
        rptaJSON = objC.leer(id)
        datos_cliente = json.loads(rptaJSON)
        # Obtiene la lista de DPTO para el combo cmbDpto
        objDpto = Departamento()
        rptaJSON = objDpto.listar()
        datos_dpto = json.loads(rptaJSON)

        objProv = Provincia()
        rptaJSON = objProv.listar_prov_dpto(
            datos_cliente['data']['departamento_id'])
        datos_provi = json.loads(rptaJSON)

        objDist = Distrito()
        rptaJSON = objDist.listar_dist_prov(
            datos_cliente['data']['provincia_id'])
        datos_distri = json.loads(rptaJSON)

        # Obtiene lista de Tipo Docs para el combo tipodoc
        objTdoc = TipoDocumento()
        rptaJSON = objTdoc.listar()
        datos_tipo_doc = json.loads(rptaJSON)

        return render_template('mantenimiento/cliente/add-edit.html', datos=session, clientes=datos_cliente, dptos=datos_dpto, provs=datos_provi, dists=datos_distri, tipo_docs=datos_tipo_doc, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_cliente.route('/clientes/delete/<id>')
def clientes_delete(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        # Instanciar la clase cliente y buscar persona_id
        objCliente = Cliente()
        rptaJSON = objCliente.buscar_id(id)
        persona_id = json.loads(rptaJSON)['data']['persona_id']
        # Instanciar la clase persona
        objPersona = Persona()
        rptaJSON = objCliente.eliminar(id)
        rptaJSON = objPersona.eliminar(persona_id)
        datos_prov = json.loads(rptaJSON)
        # Imprimir el mensaje en pantalla
        flash(datos_prov['data'])
        # Llamar a la ruta '/proveedores'
        return redirect(url_for('view_cliente.clientes'))
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
