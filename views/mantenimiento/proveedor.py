from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.mantenimiento.persona import Persona
from models.mantenimiento.proveedor import Proveedor
from models.mantenimiento.departamento import Departamento
from models.mantenimiento.provincia import Provincia
from models.mantenimiento.distrito import Distrito
from services.sunat import Sunat
# Validations
from validations.mantenimiento.proveedor import StoreProveedorRequest
import json

view_proveedor = Blueprint('view_proveedor', __name__,
                           template_folder='templates', static_folder='static')


# esta ruta se invoca desde la pagina layout.html (nav->ul)
@view_proveedor.route('/proveedores')
def proveedores():  # Listar en tabla proveedores HTML
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        objP = Proveedor()
        rptaJSON = objP.listar()
        datosP = json.loads(rptaJSON)
        return render_template('mantenimiento/proveedor/index.html', datos=session, proveedores=datosP)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_proveedor.route('/proveedores/add', methods=('GET', 'POST'))
def proveedores_add():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreProveedorRequest()  # Validations/mantenimiento/proveedor.py
        if form.validate_on_submit():  # Si es POST y  pasa las validaciones del formulario
            return proveedores_save()  # Grabar
        # Sino es POST
        # Obtiene la lista de DPTO para el combo cmbDpto
        objDpto = Departamento()
        rptaJSON = objDpto.listar()
        datos_dpto = json.loads(rptaJSON)
        return render_template('mantenimiento/proveedor/add-edit.html', datos=session, dptos=datos_dpto, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_proveedor.route('/proveedores/save', methods=['POST'])
def proveedores_save():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        if request.method == 'POST':
            # Captura de datos del formulario
            # Datos para tabla persona
            ruc = request.form['ruc']
            nombre = request.form['razon']
            direccion = request.form['direccion']
            email = request.form['email']
            celular = request.form['celular']
            distrito_id = request.form['cmbdist']
            # Datos para tabla proveedor
            id = request.form['id']  # Id de proveedor
            dni = request.form['dnirepre']
            nombre_repre = request.form['nombrerepre']
            celular_repre = request.form['celularrepre']
            email_repre = request.form['emailrepre']
            direccion_repre = request.form['direccionrepre']

            if id == '0':
                # Insertar en tabla persona
                oPersona = Persona(id, ruc, nombre=nombre, direccion=direccion,
                                   email=email, celular=celular, distrito_id=distrito_id)

                # Llamar al método insertar Persona
                rptaJSON = oPersona.insertar()
                # Llamar al método insertar Proveedor
                persona_id = json.loads(rptaJSON)['data']
                oProveedor = Proveedor(
                    id, dni, nombre_repre, direccion_repre, email_repre, celular_repre, persona_id)
                rptaJSON = oProveedor.insertar()
            else:
                # Obtener persona_id de la tabla proveedor con el id proveedor
                oProveedor = Proveedor(
                    id, dni, nombre_repre, direccion_repre, email_repre, celular_repre)
                rptaJSON = oProveedor.buscar_id(id)
                persona_id = json.loads(rptaJSON)['data']['persona_id']
                # Actualizar en tabla Persona
                oPersona = Persona(persona_id, nombre=nombre, direccion=direccion,
                                   email=email, celular=celular, distrito_id=distrito_id)
                rptaJSON = oPersona.editar()
                # Verificar si se actualizó correctamente
                if(json.loads(rptaJSON)['status']):
                    # Actualizar en tabla Proveedor
                    rptaJSON = oProveedor.editar()

            # Convertir el resultado JSON String JSON Array
            datosP = json.loads(rptaJSON)
            # Imprimir el mensaje en pantalla
            flash(datosP['data'])

            # Llamar a la ruta '/proveedores'
            return redirect(url_for('view_proveedor.proveedores'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_proveedor.route('/proveedores/edit/<id>', methods=('GET', 'POST'))
def proveedores_edit(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreProveedorRequest()
        if form.validate_on_submit():
            return proveedores_save()
        # Obtiene los datos del producto a editar
        objP = Proveedor()
        rptaJSONProvee = objP.leer(id)
        datos_provee = json.loads(rptaJSONProvee)
        # Obtiene la lista de DPTO para el combo cmbDpto
        objDpto = Departamento()
        rptaJSON = objDpto.listar()
        datos_dpto = json.loads(rptaJSON)

        objProv = Provincia()
        rptaJSONProvi = objProv.listar_prov_dpto(
            datos_provee['data']['departamento_id'])
        datos_provi = json.loads(rptaJSONProvi)

        objDist = Distrito()
        rptaJSONDist = objDist.listar_dist_prov(
            datos_provee['data']['provincia_id'])
        datos_distri = json.loads(rptaJSONDist)

        return render_template('mantenimiento/proveedor/add-edit.html', datos=session, proveedores=datos_provee, dptos=datos_dpto, provs=datos_provi, dists=datos_distri, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_proveedor.route('/proveedores/delete/<id>')
def proveedores_delete(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        # Instanciar la clase proveedor y buscar persona_id
        objProveedor = Proveedor()
        rptaJSON = objProveedor.buscar_id(id)
        persona_id = json.loads(rptaJSON)['data']['persona_id']
        # Instanciar la clase persona
        objPersona = Persona()
        rptaJSON = objProveedor.eliminar(id)
        rptaJSON = objPersona.eliminar(persona_id)
        datos_prov = json.loads(rptaJSON)
        # Imprimir el mensaje en pantalla
        flash(datos_prov['data'])
        # Llamar a la ruta '/proveedores'
        return redirect(url_for('view_proveedor.proveedores'))
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_proveedor.route('/proveedores/buscar/<ruc>')
def proveedores_buscar(ruc):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        # Instanciar la clase proveedor
        objP = Persona()
        # Buscar Proveedor en tabla persona ya que hija de tabla persona
        rptaJSON = objP.buscar(ruc)
        if(json.loads(rptaJSON)['status']):  # Si encuentra proveedor en BD
            return json.loads(rptaJSON)
        else:  # Si no encuentra proveedor
            # Consumir servicio SUNAT
            objSunat = Sunat(ruc)
            rptaJSON = objSunat.buscar_ruc()

        return json.loads(rptaJSON)

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
