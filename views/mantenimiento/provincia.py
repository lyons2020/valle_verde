from flask import Blueprint, request, session, redirect, url_for, flash
from models.mantenimiento.provincia import Provincia
import json

view_provincia = Blueprint('view_provincia', __name__,
                           template_folder='templates', static_folder='static')


@view_provincia.route('/provincias/dpto/<id>')
def provincias_dpto(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        # Obtiene la lista de DPTO para el combo cmbDpto
        objProv = Provincia()
        rptaJSON = objProv.listar_prov_dpto(id)
        datos_provi = json.loads(rptaJSON)
        return datos_provi
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
