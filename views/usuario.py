from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.usuario import Usuario
from models.mantenimiento.persona import Persona
from models.mantenimiento.departamento import Departamento
from models.mantenimiento.provincia import Provincia
from models.mantenimiento.distrito import Distrito
from models.sistema.usuario_perfil import UsuarioPerfil
from services.reniec import Reniec
from werkzeug.utils import secure_filename
from pathlib import Path
# Validations
from validations.mantenimiento.colaborador import StoreColaboradorRequest
#from validations.mantenimiento.perfil_usuario import StorePerfilUsuarioRequest
import json
import os.path

view_colaborador = Blueprint(
    'view_colaborador', __name__, template_folder='templates', static_folder='static')


@view_colaborador.route('/colaboradores')
def colaboradores():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        objU = Usuario()
        rptaJSON = objU.listar()
        datosU = json.loads(rptaJSON)
        return render_template('mantenimiento/colaborador/index.html', datos=session, colaboradores=datosU)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_colaborador.route('/colaboradores/add', methods=('GET', 'POST'))
def colaboradores_add():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreColaboradorRequest()  # Validations/mantenimiento/proveedor.py
        if form.validate_on_submit():  # Si es POST y  pasa las validaciones del formulario
            return colaboradores_save()  # Grabar
        # Sino es POST
        # Obtiene lista de DPTO para el combo cmbDpto
        objDpto = Departamento()
        rptaJSON = objDpto.listar()
        datos_dpto = json.loads(rptaJSON)
        return render_template('mantenimiento/colaborador/add-edit.html', datos=session, dptos=datos_dpto, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


# @view_colaborador.route('/colaboradores/save', methods=['POST'])
def colaboradores_save():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        if request.method == 'POST':
            # Captura de datos del formulario
            # Datos para tabla persona
            dni = request.form['dni']
            paterno = request.form['paterno']
            materno = request.form['materno']
            nombre_persona = request.form['nombre_persona']
            direccion = request.form['direccion']
            referencia = request.form['referencia']
            email = request.form['email']
            celular = request.form['celular']
            distrito_id = request.form['cmbdist']
            # Datos para tabla usuario
            id = request.form['id']  # Id de usuario
            nombre_usuario = dni

            if id == '0':
                # Insertar en tabla persona
                oPersona = Persona(id, dni, paterno, materno, nombre_persona, direccion, referencia,
                                   email, 1, celular, distrito_id, 1)

                # Llamar al método insertar Persona
                rptaJSON = oPersona.insertar()
                # Llamar al método insertar Proveedor
                persona_id = json.loads(rptaJSON)['data']
                oUsuario = Usuario(id, nombre_usuario,
                                   email, persona_id=persona_id)
                rptaJSON = oUsuario.insertar()
                usuario_id = json.loads(rptaJSON)['id']
                os.mkdir(os.path.join('static/imgs/usuario/'+str(usuario_id)))
                #os.mkdir(os.remove('static/imgs/usuario/' + id))
            else:
                # Obtener persona_id de la tabla usuario con el id usuario
                oUsuario = Usuario()
                rptaJSON = oUsuario.buscar_id(id)
                persona_id = json.loads(rptaJSON)['data']['persona_id']
                # Actualizar en tabla Persona
                oPersona = Persona(persona_id, apellido_paterno=paterno, apellido_materno=materno, nombre=nombre_persona,
                                   direccion=direccion, referencia=referencia, celular=celular, distrito_id=distrito_id)
                rptaJSON = oPersona.editar()

            # Convertir el resultado JSON String JSON Array
            datosP = json.loads(rptaJSON)
            # Imprimir el mensaje en pantalla
            flash(datosP['data'])

            # Llamar a la ruta '/colaboradores'
            return redirect(url_for('view_colaborador.colaboradores'))

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_colaborador.route('/colaboradores/edit/<id>', methods=('GET', 'POST'))
def colaboradores_edit(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = StoreColaboradorRequest()
        if form.validate_on_submit():
            return colaboradores_save()
        # Obtiene los datos del producto a editar
        objU = Usuario()
        rptaJSON = objU.leer(id)
        datos_usuario = json.loads(rptaJSON)
        # Obtiene la lista de DPTO para el combo cmbDpto
        objDpto = Departamento()
        rptaJSON = objDpto.listar()
        datos_dpto = json.loads(rptaJSON)

        objProv = Provincia()
        rptaJSON = objProv.listar_prov_dpto(
            datos_usuario['data']['departamento_id'])
        datos_provi = json.loads(rptaJSON)

        objDist = Distrito()
        rptaJSON = objDist.listar_dist_prov(
            datos_usuario['data']['provincia_id'])
        datos_distri = json.loads(rptaJSON)

        return render_template('mantenimiento/colaborador/add-edit.html', datos=session, colaboradores=datos_usuario, dptos=datos_dpto, provs=datos_provi, dists=datos_distri, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_colaborador.route('/colaboradores/delete/<id>')
def colaboradores_delete(id):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        # Instanciar la clase proveedor y buscar persona_id
        objUsuario = Usuario()
        rptaJSON = objUsuario.buscar_id(id)
        persona_id = json.loads(rptaJSON)['data']['persona_id']
        # Instanciar la clase persona
        objPersona = Persona()
        # Verificar si usuario tiene perfiles asociados
        objUsuarioP = UsuarioPerfil()
        rptaJSON = objUsuarioP.listar_perfil_x_usuario(id)
        datos = json.loads(rptaJSON)
        if(datos['status']):  # Usuario tiene perfiles
            # Imprimir el mensaje en pantalla
            flash(
                "Usuario tiene perfiles asociados a su cuenta, verificar en usuario perfil")
            # Llamar a la ruta '/colaboradores'
            return redirect(url_for('view_colaborador.colaboradores'))
        else:
            rptaJSON = objUsuario.eliminar(id)
            rptaJSON = objPersona.eliminar(persona_id)
            datos_prov = json.loads(rptaJSON)
            # Imprimir el mensaje en pantalla
            flash(datos_prov['data'])
            # Llamar a la ruta '/proveedores'
            return redirect(url_for('view_colaborador.colaboradores'))
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_colaborador.route('/colaboradores/buscar/<dni>')
def colaboradores_buscar(dni):
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        # Instanciar la clase proveedor
        objP = Persona()
        # Buscar Proveedor en tabla persona ya que hija de tabla persona
        rptaJSON = objP.buscar(dni)
        if(json.loads(rptaJSON)['status']):  # Si encuentra colaborador en BD
            return json.loads(rptaJSON)
        else:  # Si no encuentra proveedor
            # Consumir servicio Reniec
            objReniec = Reniec(dni)
            rptaJSON = objReniec.buscar_dni()

        return json.loads(rptaJSON)

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))

# Elmer Castro


@view_colaborador.route('/usuario_perfil')
def usuario_perfil():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        return render_template('mantenimiento/perfil_usuario/add-edit.html', datos=session)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))

# Elmer Castro


@view_colaborador.route('/usuario/save_perfil', methods=['POST'])
def save_perfil():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        if request.method == 'POST':
            idUsuario = request.form['txtid']
            nombre = request.form['txtnombre']
            img = request.files['txtimagen']
            nombreFile = session["img"]

            if request.files['txtimagen']:
                # Nombre que se va guardar en la tabla usuario
                nombreFile = idUsuario + '/' + img.filename
                # Eliminar imagen de usuario en la sgte dirección (static/imgs/usuario/id/ejm1.jpg)
                # session["img"] esta variable esta en View/sesion en el método def auth() 2/elmer.jpg
                if session["img"][0:1] != "0":
                    # Si usuario ya hizo un cambio de foto, entonces eliminar foto antigua
                    # para ser reemplazada por la nueva foto que viene en la variable img = request.files['txtimagen']
                    os.remove('static/imgs/usuario/' + session["img"])
                # Guardar imagen de usuario en la sgte dirección static/imgs/usuario/id/aaa.jpg
                #path = Path('static/imgs/usuario/'+idUsuario)
                # path.mkdir(parents=True)
                img.save(os.path.join('static/imgs/usuario/' +
                         idUsuario, secure_filename(img.filename)))

            # Instanciar el objeto de la clase Usuario y ejecuta los métodos
            objperfil = Usuario(id=idUsuario, nombre=nombre, img=nombreFile)

            # Llamar al método editar
            rptaJSON = objperfil.actualizar_perfil()

            # Convertir el resultado JSON String JSON Array
            datosP = json.loads(rptaJSON)

            # Imprimir el mensaje en pantalla
            flash(datosP['data'])

            # Actualizar datos de perfil usuario en variable session
            # (para que se muestre la nueva imgen o nombre de perfil por usuario)
            session['nombre'] = nombre  # Nuevo nombre usuario
            session["img"] = nombreFile  # Nueva imagen usuario

            return redirect(url_for('view_sesion.main'))
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))

# Ronald Tapia


@view_colaborador.route('/colaboradores/mostrarclave')
def colaborador_mostrarclave():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        return render_template('mantenimiento/colaborador/actualizarclave.html', datos=session)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))

# Ronald Tapia


@view_colaborador.route('/colaboradores/actualizarclave', methods=['POST'])
def colaboradores_actualizarclave():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión

        # Captura de datos del formulario
        claveactual = request.form['txtclaveactual']
        clavenueva = request.form['txtclavenueva']
        claverepetir = request.form['txtclaverepetir']

        if clavenueva == claverepetir:
            # Verificar si contraseña pertenece al usuario
            objU = Usuario()
            # session["id"]→ Views sesion (def auth():)
            rptaJSON = objU.verificarClave(claveactual, session["id"])
            datos_usuario = json.loads(rptaJSON)

            if datos_usuario['status'] == True:
                rptaJSON = objU.actualizar_clave(clavenueva, session["id"])

            # Convertir el resultado JSON String JSON Array
            datosP = json.loads(rptaJSON)

            # Imprimir el mensaje en pantalla
            flash(datosP['data'])

            # Llamar a la ruta '/'
            return render_template('mantenimiento/colaborador/actualizarclave.html', datos=session)
        else:
            flash("La clave nueva y la clave repetida son distintas")
            return render_template('mantenimiento/colaborador/actualizarclave.html', datos=session)

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
