from flask import Blueprint, render_template, request, session, redirect, url_for
from models.sesion import Sesion
from models.sistema.usuario_perfil import UsuarioPerfil
from models.sistema.menu import Menu
from wtforms.validators import ValidationError
from validations.auth.sesion import StoreSesionRequest
import json
import utils

view_sesion = Blueprint('view_sesion', __name__,
                        template_folder='templates', static_folder='static')


@view_sesion.route('/login', methods=('GET', 'POST'))
def login():
    form = StoreSesionRequest()
    if form.validate_on_submit():
        return auth()
    return render_template('login.html', form=form, disabled="readonly", ocultar="ocultar", perfiles=None)


def menuhijo(s=None, p=None):
    if s['menu_padre'] == p:
        return s['submenu']


# @view_sesion.route('/login/auth', methods=['POST'])
def auth():
    if request.method == 'POST':
        form = StoreSesionRequest()
        email = form.email.data
        clave = form.clave.data

        objSesion = Sesion(email, utils.md5_password(clave))
        rptaJSON = objSesion.iniciarSesion()
        datos = json.loads(rptaJSON)  # Convetir el JSON a un Array

        if datos["status"] == True:  # Ha iniciado correctamente la sesión
            perfil = form.perfil.data
            idUsuario = datos["data"]["id"]
            if perfil != "":
                # Verificar si perfil pertenece a usuario

                # Crear menu
                objMenu = Menu()
                rptaJSON = objMenu.listar_menu_x_perfil(perfil)
                datas = json.loads(rptaJSON)['data']
                # Lista que almacena los submenus por menu padre

                # Eliminar menus padres duplicados
                unique = {each['menu_padre']: each for each in datas}.values()
                datos_menus = json.dumps(list(unique))
                # Lista diccionario - Almacenará los menus y submenus
                my_dict = {}
                menus = []
                submenus = []
                for m in json.loads(datos_menus):
                    menu = {
                        "id": m['menu_padre'],
                        "menu": m['menu'],
                        "route": m['route_menu']
                    }
                    for s in datas:
                        if s['menu_padre'] == m['menu_padre']:
                            if s['menu_hijo'] != None:
                                submenu = {
                                    # "menu_padre": m['menu_padre'],
                                    "menu_hijo": s['menu_hijo'],
                                    "submenu": s['submenu'],
                                    "route": s['route']
                                }
                                submenus.append(submenu)
                    menu.__setitem__("submenu", submenus)
                    submenus = []
                    menus.append(menu)

                my_dict.__setitem__("menus", menus)
                print('Diccionario')
                print(my_dict)
                # Crear la sesión del usuario
                session["nombre"] = datos["data"]["nombre"]
                session["email"] = datos["data"]["email"]
                session["id"] = idUsuario
                session["img"] = datos["data"]["img"]
                session["menu"] = my_dict
                return redirect(url_for('view_sesion.main'))
            else:
                # retornar perfiles por usuario
                objUserPerfil = UsuarioPerfil()
                rptaJSON = objUserPerfil.listar_perfil_x_usuario(idUsuario)
                datos = json.loads(rptaJSON)
                print("Datos de perfiles")
                print(datos)
                if(datos['status']):
                    return render_template('login.html', form=form, disabled="", ocultar="", perfiles=datos)
                else:
                    return render_template('login.html', form=form)
        else:
            form.estadologin.data = "0"
            form.msjlogin.data = "Credenciales Incorrectas"
            return render_template('login.html', form=form)


@view_sesion.route('/main')
def main():
    if "nombre" in session:  # Significa que si ha iniciado sesión
        return render_template('menu.html', datos=session)
    else:  # No ha iniciado sesión
        return redirect(url_for('view_sesion.login'))


@view_sesion.route('/logout')
def logout():
    session.pop("nombre", None)
    session.pop("email", None)
    session.pop("id", None)
    session.pop("img", None)
    return redirect(url_for('view_sesion.login'))
