from flask import Blueprint, render_template, request, session, redirect, url_for, flash
from models.mantenimiento.almacen import Almacen
from models.mantenimiento.stock_almacen import StockAlmacen
from models.reports.report import Kardex
# Validations
from validations.reports.kardex import SearchKardexRequest
import json
from datetime import date

view_reporte = Blueprint('view_reporte', __name__,
                         template_folder='templates', static_folder='static')

# Reports


@view_reporte.route('/reporte/kardex', methods=['GET', 'POST'])
def kardex_reporte():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        form = SearchKardexRequest()
        datosV = []
        if form.validate_on_submit():
            print('di click en submit')
            almacen_id = form.almacen.data
            producto_id = form.producto.data
            date_ini = form.date_ini.data
            date_fin = form.date_fin.data
            objV = Kardex()
            rptaJSON = objV.search_kardex(
                almacen_id, producto_id, date_ini, date_fin)
            datosV = json.loads(rptaJSON)
        objA = Almacen()
        rptaJSON = objA.listar()
        datosA = json.loads(rptaJSON)
        return render_template('reports/kardex.html', datos=session, almacenes=datosA, kardexs=datosV, form=form)
    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))


@view_reporte.route('/reporte/kardex/detalle', methods=['POST'])
def kardex_reporte_detalle():
    if "nombre" in session:  # Significa que el usuario si ha iniciado sesión
        alm_id = request.form['almacen_id']
        prod_id = request.form['producto_id']
        objK = Kardex()
        # Buscar Proveedor en tabla persona ya que hija de tabla persona
        rptaJSON = objK.search_details(alm_id, prod_id)

        return json.loads(rptaJSON)

    else:  # No ha inciado sesión
        return redirect(url_for('view_sesion.login'))
