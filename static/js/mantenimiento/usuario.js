jQuery(function () {
  $(".select-2").select2()
  $(".select2-container .select2-selection--single").css({ height: "100%" })
  //Diccionario de rutas (view/mantenimiento/proveedor) @view_proveedor
  function getUri(type, value = "") {
    switch (type) {
      case "LP": //Listar provincias por dpto
        return `/provincias/dpto/${value}`
      case "LD": //Listar distritos por provincia
        return `/distritos/prov/${value}`
    }
  }
  /***
   * Selects Change
   ***/
   $("#cmbdpto").on("change", function () {
    if($(this).val()!=""){
      url = getUri("LP", this.value)
      $.ajax({
        url: url,
        success: function (r) {
          llenarCombo(r, "nombre", $("#cmbprov"))
          $('#cmbdist').empty().append(new Option("Seleccione...", ""))
        },
        error: function () {
          toastr.error("Ocurrió un error al consultar PROVINCIAS, por favor inténtelo nuevamente.")
        },
      });
    }else{
      $('#cmbprov').empty().append(new Option("Seleccione...", ""))
      $('#cmbdist').empty().append(new Option("Seleccione...", ""))
    }
  });
  $("#cmbprov").on("change", function () {
    if($(this).val()!=""){
      url = getUri("LD", this.value)
      $.ajax({
        url: url,
        success: function (r) {
          llenarCombo(r, "nombre", $("#cmbdist"))
        },
        error: function () {
          toastr.error("Ocurrió un error al consultar DISTRITOS, por favor inténtelo nuevamente.")
        },
      });
    }else{
      $('#cmbdist').empty().append(new Option("Seleccione...", ""))
    }
  });
  /***
   * Inputs Text
   ***/
  $("#dni").keyup(function (e) {
    if (!$(this).is("[readonly]")) {
      let dni = $(this).val()
      if(dni.length == 8){
        limpiarReniec()
        getDataReniec(dni,'dni-loading').then((data)=>{
          data.data.hasOwnProperty("ape_paterno")?llenarCampos(data.data):''
        }).catch(e=>console.log(e))
      }
    }
  });
  /***
   * Funciones
   ***/
  // Llenar campos persona
  function llenarCampos(data) {
    $("#paterno").val(data.ape_paterno)
    $("#materno").val(data.ape_materno)
    $("#nombre_persona").val(data.nombres)
    $("#direccion").val(data.domicilio)
  }
  // Limpiar campos relacionados a web service reniec
  function limpiarReniec(){
    $("#paterno").val("")
    $("#materno").val("")
    $("#nombre_persona").val("")
    $("#direccion").val("")
  }
});
