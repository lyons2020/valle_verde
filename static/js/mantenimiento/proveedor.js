jQuery(function () {
  $(".select-2").select2()
  $(".select2-container .select2-selection--single").css({ height: "100%" })
  //Diccionario de rutas (view/mantenimiento/proveedor) @view_proveedor
  function getUri(type, value = "") {
    switch (type) {
      case "LP": //Listar provincias por dpto
        return `/provincias/dpto/${value}`
      case "LD": //Listar distritos por provincia
        return `/distritos/prov/${value}`
    }
  }
  /***
   * Selects Change
   ***/
   $("#cmbdpto").on("change", function () {
    if($(this).val()!=""){
      url = getUri("LP", this.value)
      $.ajax({
        url: url,
        success: function (r) {
          llenarCombo(r, "nombre", $("#cmbprov"))
          $('#cmbdist').empty().append(new Option("Seleccione...", ""))
        },
        error: function () {
          toastr.error("Ocurrió un error al consultar PROVINCIAS, por favor inténtelo nuevamente.")
        },
      });
    }else{
      $('#cmbprov').empty().append(new Option("Seleccione...", ""))
      $('#cmbdist').empty().append(new Option("Seleccione...", ""))
    }
  });
  $("#cmbprov").on("change", function () {
    if($(this).val()!=""){
      url = getUri("LD", this.value)
      $.ajax({
        url: url,
        success: function (r) {
          llenarCombo(r, "nombre", $("#cmbdist"))
        },
        error: function () {
          toastr.error("Ocurrió un error al consultar DISTRITOS, por favor inténtelo nuevamente.")
        },
      });
    }else{
      $('#cmbdist').empty().append(new Option("Seleccione...", ""))
    }
  });
  /***
   * Inputs Text
   ***/
  $("#ruc").keyup(function (e) {
    if (!$(this).is("[readonly]")) {
      let ruc = $(this).val()
      if(ruc.length == 11){ 
        limpiar("ruc")
        getDataRuc(ruc,'ruc-loading').then((data)=>{
          data.data.hasOwnProperty("nombre_o_razon_social")?llenarCampos(data.data,"ruc"):''
        }).catch(e=>console.log(e))
      }
    }
  });
  $("#dnirepre").keyup(function (e) {
      let dni = $(this).val()
      if(dni.length == 8) { 
        limpiar("dni")
        getDataReniec(dni,'dnirepre-loading').then((data)=>{
          data.data.hasOwnProperty("ape_paterno")?llenarCampos(data.data,"dni"):''
        }).catch(e=>console.log(e))
      }
  });
  /***
   * Funciones
   ***/
  //Llenar campos proveedor(ruc) o persona natural (dni)
  function llenarCampos(data, tipoDoc) {
    if(tipoDoc=="ruc"){
      console.log('es ruc')
      $("#razon").val(data.nombre_o_razon_social)
      $("#direccion").val(data.direccion_completa)
    }else if(tipoDoc=="dni"){
      $("#nombrerepre").val(data.Nombre_completo)
      $("#direccionrepre").val(data.domicilio)
    }
  }
  function limpiar(tipoDoc="dni") {
    if(tipoDoc=="ruc"){
      $("#razon").val('')
      $("#direccion").val('')
    }else if(tipoDoc=="dni"){
      $("#nombrerepre").val('')
      $("#direccionrepre").val('')
    }
  }
});
