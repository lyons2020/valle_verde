jQuery(function () {
  $(".select-2").select2()
  $(".select2-container .select2-selection--single").css({ height: "100%" })
  //Diccionario de rutas (view/mantenimiento/proveedor) @view_proveedor
  function getUri(type, value = "") {
    switch (type) {
      case "LP": //Listar provincias por dpto
        return `/provincias/dpto/${value}`
      case "LD": //Listar distritos por provincia
        return `/distritos/prov/${value}`
    }
  }
  /***
   * Selects Change
   ***/
  $("#cmbdpto").on("change", function () {
    url = getUri("LP", this.value)
    $.ajax({
      url: url,
      success: function (r) {
        llenarCombo(r, "nombre", $("#cmbprov"))
      },
      error: function () {
        toastr.error("Ocurrió un error al consultar PROVINCIAS, por favor inténtelo nuevamente.")
      },
    });
  });
  $("#cmbprov").on("change", function () {
    url = getUri("LD", this.value)
    $.ajax({
      url: url,
      success: function (r) {
        llenarCombo(r, "nombre", $("#cmbdist"))
      },
      error: function () {
        toastr.error("Ocurrió un error al consultar DISTRITOS, por favor inténtelo nuevamente.")
      },
    });
  });
});
