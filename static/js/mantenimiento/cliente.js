jQuery(function () {
  $(".select-2").select2()
  $(".select2-container .select2-selection--single").css({ height: "100%" })
  //Diccionario de rutas (view/mantenimiento/cliente) @view_cliente
  function getUri(type, value = "") {
    switch (type) {
      case "LP": //Listar provincias por dpto
        return `/provincias/dpto/${value}`
      case "LD": //Listar distritos por provincia
        return `/distritos/prov/${value}`
    }
  }
  /***
   * Selects Change
   ***/
  $("#tipodoc").on("change", function(){
    $("#numero_doc").val("")
    switch($(this).val()) {
      case "1": maxlendoc(8,'Nombre *'); break
      case "2": maxlendoc(11,'Razón Social *'); break
      default: maxlendoc(0, 'Nombre *'); toastr.error("Seleccione un tipo de documento por favor."); break
    }
  })
  $("#cmbdpto").on("change", function () {
    if($(this).val()!=""){
      url = getUri("LP", this.value)
      $.ajax({
        url: url,
        success: function (r) {
          llenarCombo(r, "nombre", $("#cmbprov"))
          $('#cmbdist').empty().append(new Option("Seleccione...", ""))
        },
        error: function () {
          toastr.error("Ocurrió un error al consultar PROVINCIAS, por favor inténtelo nuevamente.")
        },
      });
    }else{
      $('#cmbprov').empty().append(new Option("Seleccione...", ""))
      $('#cmbdist').empty().append(new Option("Seleccione...", ""))
    }
  });
  $("#cmbprov").on("change", function () {
    if($(this).val()!=""){
      url = getUri("LD", this.value)
      $.ajax({
        url: url,
        success: function (r) {
          llenarCombo(r, "nombre", $("#cmbdist"))
        },
        error: function () {
          toastr.error("Ocurrió un error al consultar DISTRITOS, por favor inténtelo nuevamente.")
        },
      });
    }else{
      $('#cmbdist').empty().append(new Option("Seleccione...", ""))
    }
  });
  /***
   * Inputs Text
   ***/
  $("#numero_doc").keyup(function (e) {
    if (!$(this).is("[readonly]")) {
      let nro = $(this).val()
      if(nro.length == 8 && $("#tipodoc").val() == 1){
        limpiar()
        getDataReniec(nro,'nrodoc-loading').then((data)=>{
           data.data.hasOwnProperty("ape_paterno")?llenarCampos(data.data):''
         }).catch(e=>console.log(e))
      }else if(nro.length == 11 && $("#tipodoc").val() == 2){
        getDataRuc(nro,'nrodoc-loading').then((data)=>{
          data.data.hasOwnProperty("nombre_o_razon_social")?llenarCampos(data.data):''
        }).catch(e=>console.log(e))
      }
    }
  });
  /***
   * Funciones
   ***/
  // Según tipo Doc. seleccionado Maxlength
  function maxlendoc(max,label){
    $("#numero_doc").attr("maxlength",max)
    $('label[for="nombre"]').text(label)
  }
  //Llenar campos persona natural (dni)
  function llenarCampos(data) {
    if($("#tipodoc").val() == 1){
      $("#paterno").val(data.ape_paterno)
      $("#materno").val(data.ape_materno)
      $("#nombre").val(data.Nombre_completo)
      $("#direccion").val(data.domicilio)
    }else{
      $("#nombre").val(data.nombre_o_razon_social)
      $("#direccion").val(data.direccion_completa)
    }
  }
  //Limpiar campos relacionados a web service reniec
  function limpiar(){
    if($("#tipodoc").val() == 1){
      $("#paterno").val("")
      $("#materno").val("")
    }
    $("#nombre").val("")
    $("#direccion").val("")
  }
});
