jQuery(function () {
  $(".select-2").select2()
  $(".select2-container .select2-selection--single").css({ height: "100%" })
  //Diccionario de rutas (view/mantenimiento/proveedor) @view_proveedor
  function getUri(type, value = "") {
    switch (type) {
      case "VD": //Listar detalles de venta
        return `/venta_detalles/venta/${value}`
    }
  }
  
  /* Esquema html para tr detalle venta */
function formatDetail (r) {
  let html="";
  $.each(r['data'], function(key, value) {
      html += '<table class="table">'+
                '<thead>'+
                  '<tr class="text-center">'+
                    '<th scope="col" width="30%">Producto</th>'+
                    '<th scope="col" width="10%">Cantidad</th>'+
                    '<th scope="col" width="10%">Precio</th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody>'+
                  '<tr class="text-center">'+
                      '<td data-title="Producto">'+value["producto"]+'</td>'+
                      '<td data-title="Cantidad">'+value["cantidad"]+'</td>'+
                      '<td data-title="Precio">'+value["precio"]+'</td>'+
                  '</tr>'+
                '</tbody>'+
              '</table>'
  })
  return html;
}

 // Agregar evento listener para abrir y cerrar detalle venta
 $('#listado tbody').on('click', 'td.details-control', function () {
  let tr = $(this).closest('tr')
  let venta_id = tr.data('id')
  let row = $('#listado').DataTable().row(tr)
  if (row.child.isShown() ) {
      // Cerrar tr
      row.child.hide();
      tr.removeClass('shown');
      tr.removeClass("filaSeleccionada");
  }
  else {
    tr.addClass("filaSeleccionada");
    let url = getUri('VD',venta_id)
    $.ajax({
      url: url,
      success: function (r) {
        // Abrir tr
        row.child( formatDetail(r) ).show();
        tr.addClass('shown');
      },
      error: function () {
        toastr.error("Ocurrió un error al consultar detalle, por favor inténtelo nuevamente.")
      },
    });
  }
} );
});
