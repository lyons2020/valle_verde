jQuery(function () {
  $(".select-2").select2()
  $(".select2-container .select2-selection--single").css({ height: "100%" })
  //Diccionario de rutas (view/mantenimiento/proveedor) @view_proveedor
  function getUri(type, value = "") {
    switch (type) {
      case "LP": //Listar kardex por almacen-producto o solo por almacén (POST)
        return `/stock_almacenes/productos/${value}`
      case "KD": //Listar detalles de kardex
        return `/reporte/kardex/detalle`
    }
  }
  
  /* Esquema html para tr detalle venta */
function formatDetail (r) {
  let html="";
  $.each(r['data'], function(key, value) {
      html += '<table class="table">'+
                '<thead>'+
                  '<tr class="text-center">'+
                    '<th scope="col" width="11%">Fecha</th>'+
                    '<th scope="col" width="12%">Tipo Movimiento</th>'+
                    '<th scope="col" width="9%">Id Trans.</th>'+
                    '<th scope="col" width="9%">N. Doc.</th>'+
                    '<th scope="col" width="19%">Persona</th>'+
                    '<th scope="col" width="14%">Ingreso</th>'+
                    '<th scope="col" width="14%">Salida</th>'+
                    '<th scope="col" width="12%">Estado</th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody>'
      if (value["estado"] == 0){
        html +=   '<tr class="text-center text-danger">'
      }else{
        html +=   '<tr class="text-center">'
      }
      html +=       '<td data-title="Fecha">'+value["fecha"]+'</td>'+
                    '<td data-title="Tipo Movimiento">'+value["tipo_movimiento"]+'</td>'+
                    '<td data-title="ID Movimiento">'+value["transaccion_id"]+'</td>'+
                    '<td data-title="Nro. Doc.">'+value["nro_doc_transaccion"]+'</td>'+
                    '<td data-title="Persona">'+value["persona_nombre"]+'</td>'+
                    
                    
                    '<td data-title="Ingreso">'+value["ingreso"]+'</td>'+
                    '<td data-title="Salida">'+value["salida"]+'</td>'+
                    '<td data-title="Estado">'+value["estado"]+'</td>'+
                  '</tr>'+
                '</tbody>'+
              '</table>'
  })
  return html;
}

  // Change Almacen
  $("#almacen").on("change", function(){
    if($(this).val()!="" && Number.parseInt($(this).val())>0){
      let url = getUri('LP', $(this).val())
      $.ajax({
        url: url,
        success: function (r) {
          // rpta de view (lista de productos según almacén seleccionado)
          console.log(r)
          llenarCombo(r,"nombre", $("#producto"),"producto_id","Seleccione...","0")
        },
        error: function () {
          toastr.error("Ocurrió un error al consultar productos, por favor inténtelo nuevamente.")
        },
      });
    }else{
      $("#producto").empty().append(new Option("Seleccione Primero Almacén", "0"))
    }
  })
 // Agregar evento listener para abrir y cerrar detalle venta
 $('#listado tbody').on('click', 'td.details-control', function () {
  let tr = $(this).closest('tr')
  let alm_id = tr.data('idalm')
  let prod_id = tr.data('idprod')

  let row = $('#listado').DataTable().row(tr)
  if (row.child.isShown() ) {
      // Cerrar tr
      row.child.hide();
      tr.removeClass('shown');
      tr.removeClass("filaSeleccionada");
  }
  else {
    tr.addClass("filaSeleccionada");
    let url = getUri('KD',"")
    $.ajax({
      type: 'POST',
      url: url,
      data: {almacen_id:alm_id,producto_id:prod_id},
      success: function (r) {
        // Abrir tr
        row.child( formatDetail(r) ).show();
        tr.addClass('shown');
      },
      error: function () {
        toastr.error("Ocurrió un error al consultar detalle, por favor inténtelo nuevamente.")
      },
    });
  }
} );
});
