jQuery(function () {
  let route_delete = ""
  window.setTimeout(function () {
    $(".alert")
      .fadeTo(1000, 0)
      .slideUp(1000, function () {
        $(this).remove();
      });
  }, 3000);
  //Toast
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: true,
    positionClass: "toast-top-right",
    preventDuplicates: true,
    showDuration: "300",
    hideDuration: "1000",
    timeOut: "5000",
    extendedTimeOut: "2500",
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut",
    iconClass: "d-none",
  };
  const btnDelete = document.querySelectorAll('.btn-eliminar')

  if (btnDelete){
     const btnArray = Array.from(btnDelete);
     btnArray.forEach((btn)=>{
         btn.addEventListener('click', (e)=>{
             if(!confirm('Esta seguro de eliminarlo')){
                 e.preventDefault();
             }
         });
     });
  }
const btnDelete2 = document.querySelectorAll(".btn-eliminar2");
if (btnDelete2) {
  const btnArray = Array.from(btnDelete2);
  btnArray.forEach((btn,i) => {
    btn.addEventListener("click", (e) => {
      $("#grabarModalCenter").modal("show");
      route_delete = btnDelete2[i].getAttribute("data-href")
    });
  });
}
const btnConfirmDelete = document.querySelectorAll(".btn-confirm-delete");
if (btnConfirmDelete) {
  const btnArray = Array.from(btnConfirmDelete);
  btnArray.forEach((btn) => {
    btn.addEventListener("click", (e) => {
      $("#grabarModalCenter").modal("hide");
      console.log("Di click en si eliminar: " + route_delete)
      window.location.href = route_delete;
    });
  });
}
const btnGuardar = document.querySelectorAll('.btn-guardar')

if (btnGuardar){
   const btnArray = Array.from(btnGuardar);
   btnArray.forEach((btn)=>{
       btn.addEventListener('click', (e)=>{
           if(!confirm('Esta seguro de grabar los datos')){
               e.preventDefault();
           }
       });
   });
}

const btnGuardar2 = document.querySelectorAll(".btn-guardar2");
if (btnGuardar2) {
  const btnArray = Array.from(btnGuardar2);
  btnArray.forEach((btn) => {
    btn.addEventListener("click", (e) => {
      $("#grabarModalCenter").modal("show");
    });
  });
}
const btnConfirmSave = document.querySelectorAll(".btn-confirm-save");
if (btnConfirmSave) {
  const btnArray = Array.from(btnConfirmSave);
  btnArray.forEach((btn) => {
    btn.addEventListener("click", (e) => {
      $("#grabarModalCenter").modal("hide");
      let s = "";
      for (var i = 0; i < requiredElements.length; i++) {
        var e = requiredElements[i];
        s += e.value.length ? "" : 1;
        if(!e.value.length){
          $(`#${e.id}`).addClass("required")
          $(`#${e.id}`).next('span').find('.select2-selection').addClass('required')
        }else{
          $(`#${e.id}`).removeClass("required")
          $(`#${e.id}`).next('span').find('.select2-selection').removeClass('required')
        }
      }
      if (!s) {
        $("#cargandoModal").modal("show");
        $("#form").submit();
      }
    });
  });
}

const btnNo = document.querySelectorAll(".btn-no");
if (btnNo) {
  const btnArray = Array.from(btnNo);
  btnArray.forEach((btn) => {
    btn.addEventListener("click", (e) => {
      console.log('di click en btn no')
      $("#grabarModalCenter").modal("hide");
    });
  });
}
jQuery(".solo-numero").on("input", function (evt) {
  jQuery(this).val(
    jQuery(this)
      .val()
      .replace(/[^0-9]/g, "")
  );
});
const requiredElements = document
  .getElementById("form")?document
  .getElementById("form").querySelectorAll("[required]"):null;
})