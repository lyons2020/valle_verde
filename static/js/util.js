//Diccionario de rutas (view/mantenimiento/cliente) @view_cliente
function getUri(type, value = "") {
  switch (type) {
    case "BD": //Buscar dni en BD, consume servicio RENIEC
      return `/colaboradores/buscar/${value}`
    case "BR": //Buscar ruc en BD, sino lo encuentra consume servicio SUNAT
      return `/proveedores/buscar/${value}`
  }
}
// Transformar Data a formato select2 (id - text)
function llenarCombo(res, nombre, cmbdestino,id="id",placeholder="Seleccione...",value="") {
  const data = res.data.map((option) => {
    option.id = option[id]
    option.text = option[nombre]
    return option
  })
  cmbdestino.empty().append(new Option(placeholder, value)).select2({ data: data })
  $(".select2-container .select2-selection--single").css({ height: "100%" })
  $(cmbdestino).removeAttr("disabled")
}
// Buscar DNI, Consumir servicio RENIEC
function getDataReniec(dni, idloading) {
  url = getUri("BD", dni)
  return $.ajax({
    url: url,
    beforeSend: function () {
      $(`#${idloading}`).addClass("loading-txt")
    },
    success: function (r) {
      if (r.status) {
        //Colaborador ya se encuentra registrado
        toastr.error(
          `DNI: ${dni} - ${r.data.nombre} ya se encuentra registrado.`
        )
      } else {
        //Datos obtenidos de RENIEC
        r.data.hasOwnProperty("error") ? toastr.error(r.data.error) : r.data
      }
    },
    complete: function () {
      $(`#${idloading}`).removeClass("loading-txt")
    },
    error: function () {
      toastr.error("Ocurrió un error al consultar DNI, por favor inténtelo nuevamente o complete los campos manualmente.")
    },
  })
}
// Buscar RUC en BD sino existe Consumir servicio SUNAT
function getDataRuc(ruc, idloading) {
  url = getUri("BR", ruc)
  return $.ajax({
    url: url,
    beforeSend: function () {
      $(`#${idloading}`).addClass("loading-txt")
    },
    success: function (r) {
      if (r.status) {
        //Ruc ya se encuentra registrado
        toastr.error(
          `RUC: ${ruc} - ${r.data.nombre} ya se encuentra registrado.`
        )
      } else {
        //Datos obtenidos de SUNAT
        return r.data.hasOwnProperty("error") ? toastr.error(r.data.error) : r.data
      }
    },
    complete: function () {
      $(`#${idloading}`).removeClass("loading-txt")
    },
    error: function () {
      toastr.error("Ocurrió un error al consultar RUC, por favor inténtelo nuevamente o complete los campos manualmente.")
    },
  });
}
//Agregar Cabecera a Thead Table
function searchHeadTable(table, cols_ocultar=[],ocultar_ultima=true){
  let count_tr = ocultar_ultima ? table.find('thead tr').children().length - 1 : table.find('thead tr').children().length;
  ocultar_ultima ? $(`#${table.attr('id')} thead tr th:last`).html("") : ""
  $(`#${table.attr('id')} thead tr`).clone(true).appendTo(`#${table.attr('id')} thead`);
  $(`#${table.attr('id')} thead tr:eq(1) th`).each(function (i) {
      if (i < count_tr) {
        let existeArr = $.inArray(i, cols_ocultar);
        if(existeArr==-1){
          var title = $(this).text();
          $(this).html('<input type="text" placeholder="Search ' + title + '" />');
          $('input', this).on('keyup change', function () {
              if (table.DataTable().column(i).search() !== this.value) {
                  table.DataTable().column(i).search(this.value).draw();
              }
          });
        }
      }
  });
}